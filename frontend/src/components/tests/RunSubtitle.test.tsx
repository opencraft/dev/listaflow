import { customRender } from "../../utils/test-utils";
import { RunSubtitle } from "../RunSubtitle";
import { Run } from "../../types/Task";
import { expect, describe, it } from "vitest";

describe("RunSubtitle.tsx", () => {
  const run = {
    id: "test_run_id",
    name: "Test Run",
    is_archived: false,
    assignees: [],
    team_name: "Test Team",
    start_date: "2024-03-07T12:00:00",
    end_date: "2024-03-08T12:00:00",
    due_date: "2024-03-09T12:00:00",
    completed_checklist_count: 5,
    total_checklist_count: 10,
    recurrence: {
      checklist_definition: "test_checklist_definition_id",
      recurring_schedule_display: "Every 5 minutes",
      team: "test_team_id",
    },
  };

  it("Displays run subtitle", async () => {
    const result = customRender(<RunSubtitle run={run} />);
    expect(result.getByText("Test Team")).toBeInTheDocument();
  });
});
