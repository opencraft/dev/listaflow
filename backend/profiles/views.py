"""
Views for anything related to user profile
"""

# Create your views here.
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from drf_social_oauth2.views import ConvertTokenView
from google_auth_oauthlib.flow import Flow

from oauth2_provider.models import AccessToken, Application, RefreshToken
from oauth2_provider.settings import oauth2_settings
from oauthlib import common
from rest_condition import Or, And
from rest_framework import mixins
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_registration import signals
from rest_registration.api.views.register import process_verify_registration_data
from rest_registration.settings import registration_settings
from rest_registration.utils.responses import get_ok_response
from rest_registration.utils.users import get_user_by_verification_id
from rest_registration.verification_notifications import (
    send_register_verification_email_notification,
)

from lib.permissions import IsSafeMethod, IsSuperuser, IsAction

from profiles.models import Team, TeamMembership
from profiles.permissions import IsTeamMember
from profiles.serializers import (
    CustomVerifyRegistrationSerializer,
    ResendEmailVerificationSerializer,
    TeamSerializer,
    TeamNameSerializer,
    TeamMembershipSerializer,
)
from utils.exceptions import raise_rest_validation_error


@api_view(["POST"])
@permission_classes([AllowAny])
def resend_verification_email(request):
    """
    Resend verification email
    """
    serializer = ResendEmailVerificationSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
        user = get_user_by_verification_id(
            serializer.validated_data["id"], require_verified=False
        )
        if not user.is_active:
            send_register_verification_email_notification(request, user)
    return Response(status=200)


@api_view(["POST"])
@permission_classes([AllowAny])
def verify_registration(request):
    """
    Verify registration via signature.
    """
    user = process_verify_registration_data(
        request.data, serializer_context={"request": request}
    )
    signals.user_activated.send(sender=None, user=user, request=request)
    extra_data = None
    if registration_settings.REGISTER_VERIFICATION_AUTO_LOGIN:
        serializer = CustomVerifyRegistrationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        application = Application.objects.filter(
            client_id=serializer.data["client_id"]
        ).first()
        expires = timezone.now() + timezone.timedelta(
            seconds=oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS
        )
        access_token = AccessToken(
            user=user,
            scope="read write groups",
            expires=expires,
            token=common.generate_token(),
            application=application,
        )
        access_token.save()
        refresh_token = RefreshToken(
            user=user,
            token=common.generate_token(),
            application=application,
            access_token=access_token,
        )
        refresh_token.save()
        extra_data = {
            "access_token": access_token.token,
            "expires_in": oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS,
            "token_type": "Bearer",
            "scope": access_token.scope,
            "refresh_token": refresh_token.token,
        }
    return get_ok_response(_("User verified successfully"), extra_data=extra_data)


@api_view(["POST"])
@permission_classes([AllowAny])
def google_auth_view(request):
    """
    Custom view to fetch access token from google authorization code.
    This is required as drf_social_oauth2 expects access_token and it cannot
    use auth code to fetch it.
    """
    creds = getattr(settings, "GOOGLE_CREDENTIALS")
    scope = getattr(settings, "SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE")
    if not creds or not scope:
        raise_rest_validation_error(
            "detail", _("Google authentication is not configured properly!")
        )

    token = request.data.get("token")
    redirect_uri = request.data.get("redirect_uri")
    if not token or not redirect_uri:
        raise_rest_validation_error("detail", _("Unable to login using google"))

    # Send auth code and fetch access_token.
    flow = Flow.from_client_config(
        client_config=creds,
        scopes=scope,
        redirect_uri=redirect_uri,
    )
    flow.fetch_token(code=token)
    request.data["token"] = flow.credentials.token

    # Reuse ConvertTokenView from drf_social_oauth2 to convert this
    # access_token to application access_token.
    return ConvertTokenView().post(request)


class UserTeamViewSet(mixins.ListModelMixin, GenericViewSet):
    """User team listing view"""

    permission_classes = [IsAuthenticated, IsSafeMethod]
    serializer_class = TeamSerializer
    pagination_class = None

    def get_queryset(self):
        """
        Override to return teams of user only
        """
        if getattr(self, "swagger_fake_view", False):
            return Team.objects.all()
        user = self.request.user
        return user.teams.all()


class UserTeamNamesViewSet(mixins.ListModelMixin, GenericViewSet):
    """User team listing view"""

    permission_classes = [IsAuthenticated]
    serializer_class = TeamNameSerializer
    pagination_class = None

    def get_queryset(self):
        """
        Override to return teams of user only
        """
        if getattr(self, "swagger_fake_view", False) or self.request.user.is_superuser:
            return Team.objects.all()
        user = self.request.user
        return user.teams.all()


class TeamsViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, GenericViewSet):
    """
    Global team listing view.
    """

    permission_classes = [Or(IsSuperuser, And(IsAction("retrieve"), IsTeamMember))]
    serializer_class = TeamSerializer
    lookup_url_kwarg = "id"

    def get_queryset(self):
        return Team.objects.all()


class TeamMembershipViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    GenericViewSet,
):
    """Team member listing view"""

    permission_classes = [IsAuthenticated, Or(IsTeamMember, IsSuperuser)]
    serializer_class = TeamMembershipSerializer
    lookup_url_kwarg = "membership_id"

    def get_queryset(self):
        """
        Get the list of team members for a team.
        """
        if getattr(self, "swagger_fake_view", False):
            return TeamMembership.objects.all()
        return TeamMembership.objects.filter(team__id=self.kwargs["team_id"])
