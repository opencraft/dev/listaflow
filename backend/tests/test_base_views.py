"""
Tests for the base views of the application, not related to any Django module.
"""

import pytest
from rest_framework import status


# Note: Testing the index will be difficult without significant changes to CI,
# since the index rendering is only used locally and with a build output, which
# CI will not have access to unless we manually install NPM/node of the right version
# into the backend container. That may be worth doing, but the change this comment is
# in is big enough already.


@pytest.mark.usefixtures("api_client")
def test_handles_bad_endpoint(api_client):
    """
    Verify that supplying a bogus API endpoint returns a somewhat useful response.
    """
    client = api_client()
    response = client.get("/api/boop/", format="json")
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert response.json()["detail"] == "/api/boop/ is not a valid API Endpoint."
