import { Run } from "../types/Task";
import { Paginated } from "./Paginated";
import Container from "react-bootstrap/Container";
import { ListController } from "@opencraft/providence/base/lists/types/ListController";
import { Single } from "@opencraft/providence-redux/components/Single";
import { RunsTableRow } from "./RunsTableRow";

declare interface RunsTableArgs {
  controller: ListController<Run>;
  onArchiveUnarchive: (id: string, value: boolean) => void;
}

export const RunsTable = ({
  controller,
  onArchiveUnarchive,
}: RunsTableArgs) => {
  return (
    <Paginated hideTop controller={controller}>
      <Container className="run-table">
        {controller.list.map((singleController) => (
          <Single key={singleController.x!.id} controller={singleController}>
            {() => (
              <RunsTableRow
                run={singleController.x!}
                onArchiveUnarchive={onArchiveUnarchive}
              />
            )}
          </Single>
        ))}
      </Container>
    </Paginated>
  );
};
