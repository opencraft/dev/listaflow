import ReactMarkdown from "react-markdown";
import remarkGfm from "remark-gfm";

import { InterfaceComponentProps } from "../../types/Interaction";
import components from "../ReactMarkdownOverrides";

export const MarkdownTextarea = ({ response }: InterfaceComponentProps) => {
  const displayedValue = response.model ? response.model.content : "";

  return (
    <div dir="auto">
      <ReactMarkdown
        children={displayedValue}
        remarkPlugins={[remarkGfm]}
        components={components}
      />
    </div>
  );
};
