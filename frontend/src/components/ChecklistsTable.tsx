import { TaskListReview } from "../types/Task";
import { Paginated } from "./Paginated";
import Container from "react-bootstrap/Container";
import ArchiveIcon from "../assets/icons/archive.svg?react";
import UnarchiveIcon from "../assets/icons/unarchive.svg?react";
import { Col, Form, InputGroup, Nav, Row } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { ListController } from "@opencraft/providence/base/lists/types/ListController";
import { useEffect, useState } from "react";
import { ChecklistTableRow } from "./ChecklistTableRow";
import { Single } from "@opencraft/providence-redux/components/Single";

declare interface ChecklistsTableArgs {
  controller: ListController<TaskListReview>;
  isArchived?: boolean;
  currentUsername: string;
  onSingleArchiveUnarchive: (id: string, value: boolean) => void;
  onBulkArchiveUnarchive: (ids: string[], value: boolean) => void;
}

export const ChecklistsTable = ({
  controller,
  isArchived,
  currentUsername,
  onSingleArchiveUnarchive,
  onBulkArchiveUnarchive,
}: ChecklistsTableArgs) => {
  const { t } = useTranslation();

  const archivedColSizes = {
    xs: [10, 0, 0, 2],
    sm: [8, 0, 0, 4],
    md: [4, 5, 0, 3],
    lg: [4, 6, 0, 2],
  };
  const activeColSizes = {
    xs: [6, 0, 4, 2],
    sm: [5, 0, 3, 4],
    md: [4, 3, 2, 3],
    lg: [4, 3, 3, 2],
  };
  const colSizes = isArchived ? archivedColSizes : activeColSizes;
  const headingColSizes = colSizes;

  const [selectedChecklist, setSelectedChecklist] = useState(new Set<string>());
  const anySelected = selectedChecklist.size > 0;
  const allSelected = selectedChecklist.size === controller.list.length;

  const setChecklistSelectState = (checklistId: string, isChecked: boolean) => {
    if (selectedChecklist.has(checklistId) === isChecked) {
      return;
    }
    if (isChecked) {
      selectedChecklist.add(checklistId);
    } else {
      selectedChecklist.delete(checklistId);
    }
    setSelectedChecklist(new Set(selectedChecklist));
  };

  const setAllSelectState = (checked: boolean) => {
    controller.list.forEach((singleController) => {
      setChecklistSelectState(singleController.x!.id, checked);
    });
  };

  const operateChangingIsArchive = () => {
    const ids: string[] = Array.from(selectedChecklist).slice();
    onBulkArchiveUnarchive(ids, !isArchived);
  };

  /** Hooks */
  useEffect(() => {
    const selected = Array.from(selectedChecklist).slice();
    for (const id of selected) {
      if (
        controller.list.filter((single) => single.x!.id === id).length === 0
      ) {
        selectedChecklist.delete(id);
      }
    }
    setSelectedChecklist(selectedChecklist);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [controller.list]);
  return (
    <Paginated hideTop controller={controller}>
      <Container className="checklist-table">
        <Row className="position-sticky top-n1 mx-0 heading z-index-sticky">
          <Col
            xs="auto"
            className={`py-4 ps-0 list-checkbox-header ${anySelected ? "checked" : ""}`}
          >
            <InputGroup
              className={`switchable ${anySelected ? "" : "not-checked"}`}
            >
              <InputGroup.Text className="p-0 m-auto">
                <Form.Check.Input
                  className={`sm cursor-pointer ${allSelected ? "" : "minus"}`}
                  disabled={controller.list.length === 0}
                  type="checkbox"
                  checked={anySelected}
                  onChange={() => setAllSelectState(!anySelected)}
                />
              </InputGroup.Text>
            </InputGroup>
          </Col>
          <Col>
            <Row className="py-4 fs-sm">
              <Col
                className="ps-2"
                xs={headingColSizes.xs[0]}
                sm={headingColSizes.sm[0]}
                md={headingColSizes.md[0]}
                lg={headingColSizes.lg[0]}
              >
                <div
                  className={`list-checkbox-header-next ${anySelected ? "checked" : ""}`}
                >
                  {anySelected ? (
                    <InputGroup>
                      <InputGroup.Text
                        as={Nav.Link}
                        onClick={() => operateChangingIsArchive()}
                        className="p-2 m-n2 lh-1 white with-border"
                      >
                        {isArchived ? (
                          <UnarchiveIcon className="base-icon sm me-2 path white" />
                        ) : (
                          <ArchiveIcon className="base-icon sm me-2 path white" />
                        )}
                        <small>
                          {isArchived
                            ? t("checklists.action.unarchive")
                            : t("checklists.action.archive")}
                        </small>
                      </InputGroup.Text>
                    </InputGroup>
                  ) : (
                    t("checklists.table.heading.name")
                  )}
                </div>
              </Col>
              <Col
                className="d-none d-md-block"
                xs={headingColSizes.xs[1]}
                sm={headingColSizes.sm[1]}
                md={headingColSizes.md[1]}
                lg={headingColSizes.lg[1]}
              >
                {t("checklists.table.heading.team")}
              </Col>
              {!isArchived && (
                <Col
                  xs={headingColSizes.xs[2]}
                  sm={headingColSizes.sm[2]}
                  md={headingColSizes.md[2]}
                  lg={headingColSizes.lg[2]}
                >
                  {t("checklists.table.heading.status")}
                </Col>
              )}
              <Col
                xs={headingColSizes.xs[3]}
                sm={headingColSizes.sm[3]}
                md={headingColSizes.md[3]}
                lg={headingColSizes.lg[3]}
              >
                {t("checklists.table.heading.due")}
              </Col>
            </Row>
          </Col>
        </Row>
        {controller.list.length > 0 ? (
          controller.list.map((singleController) => (
            <Single key={singleController.x!.id} controller={singleController}>
              {() => (
                <ChecklistTableRow
                  checklist={singleController.x!}
                  selectedChecklist={selectedChecklist}
                  setChecklistSelectState={setChecklistSelectState}
                  isArchived={!!isArchived}
                  currentUsername={currentUsername}
                  onSingleArchiveUnarchive={onSingleArchiveUnarchive}
                  colSizes={colSizes}
                />
              )}
            </Single>
          ))
        ) : (
          <Row className="me-0 ms-0 fs-sm">
            <Col className="text-center">
              {t("checklists.message.noChecklist")}
            </Col>
          </Row>
        )}
      </Container>
    </Paginated>
  );
};
