/// <reference types="vite-plugin-svgr/client" />
/// <reference types="vite/client" />
interface ImportMetaEnv {
  readonly REACT_APP_CLIENT_ID: string;
  readonly REACT_APP_CLIENT_SECRET: string;
}
