# Deployments

## Frontend - CloudFlare

### Deployment through CLI

To deploy the frontend to CloudFlare Pages, make sure the project exists and run:

```
$ make build.frontend
$ wrangler login
$ npx wrangler pages publish --project "listaflow" build
```

### CI/CD Pipelines

The deployment can happen through CI/CD pipelines. In that case, make sure you set the following environment variables:

| Variable name           | Environments   | Description                                      |
| ----------------------- | -------------- | ------------------------------------------------ |
| `CLOUDFLARE_ACCOUNT_ID` | All            | CloudFlare account id                            |
| `CLOUDFLARE_API_TOKEN`  | All            | CloudFlare API token belonging to the account id |
| `CLOUDFLARE_PROJECT`    | Staging & Prod | CloudFlare project name                          |

### CloudFlare Configuration

Set the following environment variables in CloudFlare:

| Variable name                                | Environments   | Description                          |
| -------------------------------------------- | -------------- | ------------------------------------ |
| `REACT_APP_API_BASE_URL`                     | Staging & Prod | Listaflow backend URL                |
| `REACT_APP_CLIENT_ID`                        | Staging & Prod | Listaflow client id                  |
| `REACT_APP_CLIENT_SECRET`                    | Staging & Prod | Listaflow client secret              |
| `REACT_APP_GOOGLE_CLIENT_ID`                 | Staging & Prod | Listaflow Google client id           |
| `REACT_APP_TOS_LINK`                         | Staging & Prod | Link to the terms of services.       |
| `REACT_APP_PRIVACY_LINK`                     | Staging & Prod | Link to the privacy policy.          |
| `REACT_APP_MATOMO_TAG_MANAGER_CONTAINER_URL` | Staging & Prod | Matamo analytics URL for Tag Manager |

### Drafting Frontend Release

To deploy listaflow we need to create a git tag matching the regexes below:

#### Deploying to Stage

- `/^(0|[1-9]\d+)\.(0|[1-9]\d*)\.(0|[1-9]\d+)(-rc)(\.\d+)?$/`

#### Deploying to Production

- `/^(0|[1-9]\d+)\.(0|[1-9]\d*)\.(0|[1-9]\d+)$/`

Once a release is made the frontend deployment takes place automatically.

## Backend Deployment

Listaflow's backend is deployed using [Helm](https://helm.sh/docs/intro/quickstart/)
on a Kubernetes Cluster.

Helm chart and additional plugins can be [installed by following the instructions.](https://gitlab.com/opencraft/ops/helm-charts/-/blob/main/README.md#prerequisites)

Now one can install and configure Listaflow [using Listaflow's Helm Chart](https://gitlab.com/opencraft/ops/helm-charts/-/tree/main/listaflow).

### To deploy the backend and draft the release:

- Change the version of helm chart and docker image [here.](https://gitlab.com/opencraft/ops/helm-charts/-/blob/main/listaflow/Chart.yaml#L10-14)

- Follow steps in [readme](https://gitlab.com/opencraft/ops/helm-charts/-/tree/main#opencraft-helm-charts) to update chart.

- Then update version in infrastructure repo [here](https://gitlab.com/opencraft/ops/infrastructure/-/blob/main/resources/applications.tf#L14)
- Finally run `terraform apply` in infrastructure/(production/staging) repo. Although, this step currently fails due to config issues which then requires us to run

```bash
$ terraform destroy -target "module.infrastructure.module.listaflow[0].helm_release.app" && terraform apply -target "module.infrastructure.module.listaflow[0].helm_release.app"
```
