# Troubleshooting

This document goes over some common techniques for troubleshooting Listaflow when it doesn't behave as expected.

## Celery Tasks

Most issues with Listaflow are a variation of this problem. Recent improvements to the codebase are expected to reduce the incidence rate of problems with Celery tasks, but there are some ways that Celery can fail that are not easy to prevent.

For example, we use Redis as the backend for Celery tasks. Redis's data storage is ephemeral by default. We could enable persistence for Redis' storage, but this could cause other problems. For example, if Celery tasks are backed up and stored, fixing whatever prevented Celery tasks from running could cause multiple repetitive email reminders to all go out at once!

Instead, if Celery tasks have been backed up for a long time, it's advised to restart Redis, then restart the Celery workers once Redis is fully operational. After this has happened, perform any remedial tasks as necessary-- such as [manually creating runs](#creating-runs-manually), or [sending reminders](#sending-reminder-emails-manually).

To avoid being caught by surprise, configure [Dead Man's Snitch](#dead-mans-snitch-support). To keep an eye on the system's activity, check the [audit logs](#audit-logs)

## Creating Runs Manually

If a run has failed to create for a recurrence, you can create it manually by going into the Django admin, selecting the recurrence, and then using the action dropdown to select 'forcibly create next run.'

**Note:** You should not do this for recurrences managed by SprintCraft's webhooks. These MUST be initiated through SprintCraft to make sure their data is sane.

## Sending Reminder Emails Manually

If a reminder has not sent for a run, you can send the reminders manually by going into the Django admin, selecting the run, and then using the action dropdown to select 'Forcibly send reminder email'. Note that this will have no effect if the [Celery queue is stuck](#celery-tasks), so check that first.

## Dead Man's Snitch Support

Listaflow supports [Dead Man's Snitch](https://deadmanssnitch.com/) (or compatible service) as an early morning for when Celery tasks may not be working. Each hour, it will run a celery task to fetch a given URL. This can be used to 'check in' and let the service know that Listaflow is still processing Celery tasks.

This can be configured via the `DEAD_MANS_SNITCH_URL` setting via an environment variable of the same name.

## Audit Logs

To better audit Listaflow's behavior, it comes with Audit logs. You can view ALL audit logs by visiting the Audit Logs listing in the Django admin.

To easily view the audit logs for a particular Recurrence or Run, simply visit that Recurrence or Run in the Django admin. Logs will be at the bottom of the page.

Audit logs are cleared out periodically. By default, audit logs older than 60 days are removed, daily. You can configure the number of days that audit logs are retained by setting the `AUDIT_LOG_PRESERVATION_DAYS` setting via the environment variable of the same name. If set to `0`, audit logs will be preserved indefinitely.
