import React, { ReactNode } from "react";
import { render, RenderOptions } from "@testing-library/react";
import {
  defaultContextValues,
  ProvidenceContext,
} from "@opencraft/providence-redux/context";
import { Provider } from "react-redux";
import { createStore } from "redux-dynamic-modules";
import { fieldLength, requiredField } from "./validators";
import { MemoryRouter, Route, Routes } from "react-router-dom";
import { ContextRenderOptions } from "@opencraft/providence-redux/testHelpers";
import MockAdapter from "axios-mock-adapter";
import { axiosInstance } from "./helpers";
import { TestAuthProvider } from "../context/AuthProvider";
import { AuthData, User } from "../types/User";
import { Run } from "../types/Task";

export const testContext = () => {
  const defaultContext = defaultContextValues();
  defaultContext.validators.required = requiredField;
  defaultContext.validators.length = fieldLength;
  defaultContext.client.netCall = (options) => axiosInstance.request(options);
  return defaultContext;
};

declare interface PathSpec {
  path: string;
  ui: ReactNode;
}

declare type RawPathSpec = string | { path: string; ui: ReactNode };

export interface AppRenderValues {
  initialEntries?: string[];
  paths?: RawPathSpec[];
  user?: AuthData;
}

const toPathSpecs = (rawSpecs: RawPathSpec[], children): PathSpec[] =>
  rawSpecs.map((rawSpec) => {
    if (typeof rawSpec === "string") {
      return { path: rawSpec, ui: children };
    }
    return rawSpec;
  });

export const customRender = (
  ui: ReactNode,
  options: Omit<RenderOptions, "wrapper"> &
    ContextRenderOptions &
    AppRenderValues = {},
) => {
  const defaultContext = testContext();
  const { context, store } = options;
  const builtContext = { ...defaultContext, ...context };
  const targetStore = store || createStore({});
  const initialEntries = options.initialEntries ?? ["/"];
  const paths: PathSpec[] = toPathSpecs(options.paths ?? ["/"], ui);
  const user = options.user === undefined ? { username: "fox" } : options.user;
  const Wrapped = () => (
    <TestAuthProvider user={user}>
      <Provider store={targetStore}>
        <ProvidenceContext.Provider value={builtContext}>
          <MemoryRouter initialEntries={initialEntries}>
            <Routes>
              {paths.map((pathSpec) => (
                <Route
                  path={pathSpec.path}
                  element={pathSpec.ui}
                  key={pathSpec.path}
                />
              ))}
            </Routes>
          </MemoryRouter>
        </ProvidenceContext.Provider>
      </Provider>
    </TestAuthProvider>
  );
  return render(<Wrapped />);
};

export * from "@testing-library/react";
export { customRender as render };

const scheduler =
  typeof setImmediate === "function" ? setImmediate : setTimeout;

export const flushPromises = () => {
  return new Promise(function (resolve) {
    scheduler(resolve);
  });
};

export const mockAxios = new MockAdapter(axiosInstance, {
  onNoMatch: "throwException",
});

export const genUser = (overrides?: Partial<User>): User => ({
  username: "fox",
  email: "fox@opencraft.com",
  id: 1,
  ...overrides,
});

export const genRun = (overrides?): Run => {
  return {
    id: "test_run_id",
    start_date: "2024-03-07T12:00:00Z",
    end_date: "2024-03-08T12:00:00Z",
    due_date: "2024-03-07",
    name: "Test Run",
    team_name: "Test Team",
    is_archived: false,
    completed_checklist_count: 0,
    total_checklist_count: 1,
    assignees: [],
    recurrence: {
      id: "test_recurrence_id",
      periodic_task: 3,
      recurring_schedule_display: "Every 5 minutes",
      team: "test_team_id",
      checklist_definition: "test_checklist_definition_id",
    },
    ...overrides,
  };
};
