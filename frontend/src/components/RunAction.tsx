import { Dropdown, Nav } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import ThreeDotMenuIcon from "../assets/icons/three-dot-menu.svg?react";
import EditIcon from "../assets/icons/edit.svg?react";
import ArchiveIcon from "../assets/icons/archive.svg?react";
import UnarchiveIcon from "../assets/icons/unarchive.svg?react";
import ShareListIcon from "../assets/icons/share-list.svg?react";
import ListIcon from "../assets/icons/list.svg?react";
import { Run } from "../types/Task";
import useNotification from "../hooks/useNotification";
import { useTranslation } from "react-i18next";

export declare interface RunActionArgs {
  onArchiveUnarchive: (id: string, isArchived: boolean) => void;
  run: Run;
  taskReportLink: string;
}

export const RunAction = ({
  run,
  onArchiveUnarchive,
  taskReportLink,
}: RunActionArgs) => {
  const { setNotification } = useNotification();
  const { t } = useTranslation();
  const copyToClipboard = (text: string) => {
    navigator.clipboard.writeText(text);
    setNotification({
      message: t("runs.notification.copiedToClipboard"),
      variant: "success",
    });
  };
  return (
    <Dropdown className="h-100 d-flex align-items-center run-action">
      <Dropdown.Toggle
        className="d-flex align-items-center circle-active ms-auto interaction"
        as={Nav.Link}
        to="#"
        split
        id="dropdown-split-basic"
      >
        <ThreeDotMenuIcon className="base-icon" />
      </Dropdown.Toggle>
      <Dropdown.Menu className="p-2 border fs-sm">
        {!run.is_archived && (
          <Dropdown.Item
            className="text-reset"
            as={NavLink}
            to={`/runs/${run.id}/`}
          >
            <EditIcon className="base-icon sm me-2" />
            <span className="mb-0">{t("runs.action.view")}</span>
          </Dropdown.Item>
        )}
        {!run.is_archived && (
          <Dropdown.Item
            className="text-reset"
            onClick={() =>
              copyToClipboard(`${window.location.origin}/runs/${run.id}/`)
            }
          >
            <ShareListIcon className="base-icon sm me-2" />
            <span className="mb-0">{t("runs.action.copyLink")}</span>
          </Dropdown.Item>
        )}
        {taskReportLink !== "" && (
          <Dropdown.Item
            as={NavLink}
            className="text-reset"
            to={taskReportLink}
          >
            <ListIcon className="base-icon sm me-2" />
            <span className="mb-0">{t("runs.action.showReport")}</span>
          </Dropdown.Item>
        )}
        <Dropdown.Item
          className="text-reset"
          onClick={() => onArchiveUnarchive(run.id, !run.is_archived)}
        >
          {run.is_archived ? (
            <UnarchiveIcon className="base-icon sm me-2" />
          ) : (
            <ArchiveIcon className="base-icon sm me-2" />
          )}
          <span className="mb-0">
            {run.is_archived
              ? t("runs.action.unarchive")
              : t("runs.action.archive")}
          </span>
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};
