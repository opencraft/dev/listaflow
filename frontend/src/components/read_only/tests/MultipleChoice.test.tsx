import { MultipleChoice } from "../MultipleChoice";
import { render } from "../../../utils/test-utils";
import { describe, it, expect } from "vitest";

describe("MultipleChoice.tsx", () => {
  it("Renders a multiple choice response.", () => {
    const result = render(
      <MultipleChoice
        customizationArgs={null}
        response={{ model: { selected_choices: ["Choice A", "Choice B"] } }}
      />,
    );
    expect(result.queryByText("Choice A")).toBeInTheDocument();
    expect(result.queryByText("Choice B")).toBeInTheDocument();
    expect(result.queryByText("taskItem.noAnswer")).not.toBeInTheDocument();
  });

  it("Renders an empty multiple choice response.", () => {
    const result = render(
      <MultipleChoice
        customizationArgs={null}
        response={{ model: { selected_choices: [] } }}
      />,
    );
    expect(result.queryByText("taskItem.noAnswer")).toBeInTheDocument();
  });
});
