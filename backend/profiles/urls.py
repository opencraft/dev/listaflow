"""
Listaflow profile URL Configuration
"""

from django.urls import path
from django.urls.conf import include

from rest_framework_nested import routers

from profiles.views import (
    google_auth_view,
    resend_verification_email,
    verify_registration,
    UserTeamViewSet,
    UserTeamNamesViewSet,
    TeamMembershipViewSet,
    TeamsViewSet,
)


user_info_router = routers.SimpleRouter(r"")
app_name = "profiles"

team_router = routers.SimpleRouter()
team_router.register(r"teams", TeamsViewSet, basename="teams")

team_membership_router = routers.NestedSimpleRouter(
    team_router,
    "teams",
    lookup="team",
)

team_membership_router.register(
    "memberships",
    TeamMembershipViewSet,
    basename="memberships",
)

urlpatterns = [
    path("account/", include("rest_registration.api.urls")),
    path(
        "account/teams/", UserTeamViewSet.as_view({"get": "list"}), name="account-teams"
    ),
    path("", include(team_router.urls)),
    path("", include(team_membership_router.urls)),
    path(
        "account/team-names/",
        UserTeamNamesViewSet.as_view({"get": "list"}),
        name="account-team-names",
    ),
    path(
        "resend_verification_email/",
        resend_verification_email,
        name="resend-verification-email",
    ),
    path("verify-registration/", verify_registration, name="verify-registration"),
    path("google-convert-token/", google_auth_view, name="google-convert-token"),
]
