"""
Tests for workflow module.
"""

from unittest.mock import Mock
import pytest
from django.utils import timezone

from workflow.enums import ChecklistStatus
from workflow.filters import ChecklistFilter, RunChecklistFilter, RunFilter
from workflow.models import Checklist, Run
from workflow.tests.utils import (
    create_multiple_team_recurrences,
    create_team_and_checklist_definition,
)
from workflow.tests.factories import (
    ChecklistDefinitionFactory,
    ChecklistTaskDefinitionFactory,
    RecurrenceFactory,
)

from profiles.tests.factories import UserFactory

pytestmark = [
    pytest.mark.django_db(transaction=True),
]


# pylint: disable=too-many-locals
def test_checklist_filter_statuses():
    """Test filter checklist by statuses"""
    # Setup
    team, definition, _task_definitions = create_team_and_checklist_definition(
        team_member_count=1,
        tasks_kwargs=[{}, {}],
    )

    request = Mock()
    request.user = team.members.first()

    recurrence = RecurrenceFactory.create(team=team, checklist_definition=definition)
    upcoming_run, _, in_progress_run, past_due_run, completed_run = tuple(
        (recurrence.create_run() for _ in range(5))
    )
    upcoming_run.start_date = upcoming_run.start_date - timezone.timedelta(2)
    upcoming_run.end_date = upcoming_run.end_date + timezone.timedelta(2)
    upcoming_run.due_date = upcoming_run.end_date + timezone.timedelta(1)
    upcoming_run.save()
    for checklist in in_progress_run.checklists.all():
        task = checklist.active_tasks().first()
        task.completed = True
        task.save()

    yesterday_dt = timezone.now() - timezone.timedelta(days=1)
    past_due_run.end_date = yesterday_dt
    past_due_run.due_date = yesterday_dt
    past_due_run.save()

    for checklist in completed_run.checklists.all():
        checklist.completed = True
        checklist.save()

    queryset = Checklist.objects.get_queryset()
    checklist_filter = ChecklistFilter(request=request)

    # Action
    query = f"{ChecklistStatus.UP_COMING}"
    assert checklist_filter.filter_statuses(queryset, "", query).count() == 1
    query = f"{ChecklistStatus.TO_DO}"
    assert checklist_filter.filter_statuses(queryset, "", query).count() == 2
    query = f"{ChecklistStatus.PAST_DUE}"
    assert checklist_filter.filter_statuses(queryset, "", query).count() == 1
    query = f"{ChecklistStatus.IN_PROGRESS}"
    assert checklist_filter.filter_statuses(queryset, "", query).count() == 1
    query = f"{ChecklistStatus.COMPLETED}"
    assert checklist_filter.filter_statuses(queryset, "", query).count() == 1
    query = ",".join(ChecklistStatus.names())
    assert checklist_filter.filter_statuses(queryset, "", query).count() == 5


def test_checklist_filter_assignees_usernames():
    """Test filter by assignees usernames"""
    user1 = UserFactory.create(username="dummy_user_1")
    user2 = UserFactory.create(username="dummy_user_2")
    user3 = UserFactory.create(username="dummy_user_3")

    checklist_definition = ChecklistDefinitionFactory.create()
    ChecklistTaskDefinitionFactory(checklist_definition=checklist_definition)

    Checklist.create_checklist_and_corresponding_tasks(user1, checklist_definition)
    Checklist.create_checklist_and_corresponding_tasks(user2, checklist_definition)
    Checklist.create_checklist_and_corresponding_tasks(user3, checklist_definition)

    queryset = Checklist.objects.get_queryset()
    checklist_filter = ChecklistFilter(queryset=queryset)

    query = user1.username
    assert checklist_filter.filter_usernames(queryset, "", query).count() == 1

    query = ",".join([user1.username, user2.username])
    assert checklist_filter.filter_usernames(queryset, "", query).count() == 2

    query = ",".join([user1.username, user2.username, user3.username])
    assert checklist_filter.filter_usernames(queryset, "", query).count() == 3


def test_run_filter_name():
    """Test filter run by name"""
    team, definition, _task_definitions = create_team_and_checklist_definition(
        team_member_count=2,
        tasks_kwargs=[{}, {}],
    )
    recurrence = RecurrenceFactory.create(team=team, checklist_definition=definition)
    for _ in range(2):
        recurrence.create_run()

    qs = Run.objects.get_queryset()
    run_filter = RunFilter()

    name = definition.name
    assert run_filter.filter_name(qs, "", name).distinct().count() == 2
    assert run_filter.filter_name(qs, "", name.lower()).distinct().count() == 2
    fail_name = f"{name}_will_not_match"
    assert run_filter.filter_name(qs, "", fail_name).distinct().count() == 0


def test_run_filter_teams_usernames():
    """Test filter by teams or usernames"""
    # pylint: disable=unbalanced-tuple-unpacking
    recurrence_first, recurrence_second = create_multiple_team_recurrences(
        ["first", "second"]
    )
    create_team_and_checklist_definition(
        username_prefix="third",
        team_member_count=2,
    )
    for _ in range(2):
        recurrence_first.create_run()
        recurrence_second.create_run()
    recurrence_second.create_run()

    qs = Run.objects.get_queryset()

    run_filter = RunFilter({"usernames": "first_user_0"})
    assert run_filter.is_valid()
    assert run_filter.filter_queryset(qs).distinct().count() == 2

    run_filter = RunFilter({"usernames": "first_user_1"})
    assert run_filter.is_valid()
    assert run_filter.filter_queryset(qs).distinct().count() == 2

    run_filter = RunFilter({"usernames": "third_user_0"})
    assert run_filter.is_valid()
    assert run_filter.filter_queryset(qs).distinct().count() == 0

    run_filter = RunFilter({"usernames": "first_user_0,third_user_0"})
    assert run_filter.is_valid()
    assert run_filter.filter_queryset(qs).distinct().count() == 2

    run_filter = RunFilter({"usernames": "second_user_0"})
    assert run_filter.is_valid()
    assert run_filter.filter_queryset(qs).distinct().count() == 3

    run_filter = RunFilter({"usernames": "second_user_0,first_user_1"})
    assert run_filter.is_valid()
    assert run_filter.filter_queryset(qs).distinct().count() == 5

    run_filter = RunFilter(
        {"usernames": "second_user_0", "teams": recurrence_first.team.id}
    )
    assert run_filter.is_valid()
    assert run_filter.filter_queryset(qs).distinct().count() == 5

    run_filter = RunFilter(
        {"usernames": "second_user_0", "teams": recurrence_second.team.id}
    )
    assert run_filter.is_valid()
    assert run_filter.filter_queryset(qs).distinct().count() == 3


def test_run_checklist_filter_usernames():
    """Test run checklists filter by usernames"""
    # pylint: disable=unbalanced-tuple-unpacking
    team, definition, _task_definitions = create_team_and_checklist_definition(
        team_member_count=2,
        tasks_kwargs=[{}, {}],
    )
    recurrence = RecurrenceFactory.create(team=team, checklist_definition=definition)
    run = recurrence.create_run()

    qs = Checklist.objects.filter(run=run)
    checklist_filter = RunChecklistFilter()
    assert checklist_filter.filter_usernames(qs, "", "default_user_0").count() == 1
