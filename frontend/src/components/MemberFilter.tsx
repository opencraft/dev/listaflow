import { useList } from "@opencraft/providence-redux/hooks";
import { TEAMS_API } from "../constants/api-urls";
import { Team, User } from "../types/User";
import { useEffect, useState } from "react";
import Select, { MultiValue, ActionMeta, Options } from "react-select";
import { useTranslation } from "react-i18next";
import { SelectToggleDropdownIndicator } from "./react-select/SelectToggleDropdownIndicator";
import { ValueCountFunc } from "./react-select/types";
import { SelectCheckboxDropdownItemOption } from "./react-select/SelectCheckboxDropdownItemOption";
import { SelectDropdownContainer } from "./react-select/SelectDropdownContainer";
import { dropdownSelectStyles } from "./react-select/styles";
import { SelectDropdownMenu } from "./react-select/SelectDropdownMenu";
import { SelectDropdownMenuList } from "./react-select/SelectDropdownMenuList";

export enum MemberOptionType {
  TEAM = "team",
  MEMBER = "member",
}

interface TeamOption {
  type: MemberOptionType.TEAM;
  label: string;
  value: string;
  members: User[];
}

export interface MemberOption {
  type: MemberOptionType.MEMBER;
  label: string;
  value: string;
}

interface GroupedOption {
  readonly label: string;
  readonly options: readonly MemberCustomOption[];
}

export type MemberCustomOption = TeamOption | MemberOption;

const groupMemberOptions = (
  teams: Team[],
  teamLabel: string = "Team",
  memberLabel: string = "Member",
  showTeams: boolean,
): GroupedOption[] => {
  const usernames: Set<string> = new Set();
  teams
    .map((t) => t.members)
    .flat()
    .forEach((member) => usernames.add(member.username));
  const groupedOptions = [];
  if (showTeams) {
    const teamOptions = teams.map((t) => ({
      label: t.name,
      value: t.id,
      type: MemberOptionType.TEAM,
      members: t.members,
    }));
    groupedOptions.push({
      label: teamLabel,
      options: teamOptions,
    });
  }
  const memberOptions = Array.from(usernames).map(
    (username) =>
      ({
        label: username,
        value: username,
        type: MemberOptionType.MEMBER,
      }) as MemberOption,
  );
  groupedOptions.push({
    label: memberLabel,
    options: memberOptions,
  });
  return groupedOptions;
};

declare interface MemberFilterArgs {
  className?: string;
  onChange: (
    newValue: MultiValue<MemberCustomOption>,
    actionMeta: ActionMeta<MemberCustomOption>,
  ) => void;
  selectedMemberUsernames: Array<string>;
  selectedTeamIds?: Array<string>;
  isLoading?: boolean;
  showTeams?: boolean;
}

const selectedMemberCountFunc: ValueCountFunc = (
  value: Options<MemberCustomOption>,
): number => {
  if (!value) {
    return 0;
  }

  if (!!value && !("length" in value)) {
    return 1;
  }

  const values = value as MultiValue<MemberCustomOption>;
  return values.filter((o) => o.type === MemberOptionType.MEMBER).length;
};

export const MemberFilter = ({
  className,
  onChange,
  selectedMemberUsernames,
  selectedTeamIds,
  isLoading,
  showTeams,
}: MemberFilterArgs) => {
  const { t } = useTranslation();
  if (showTeams === undefined) {
    showTeams = true;
  }
  const teamController = useList<Team>("team", {
    endpoint: TEAMS_API,
    paginated: false,
  });
  teamController.getOnce();
  const [members, setMembers] = useState<Set<User>>(new Set());
  const groupedOptions = groupMemberOptions(
    teamController.list.map((c) => c.x!),
    t("common.team"),
    t("common.member"),
    showTeams,
  );
  useEffect(() => {
    const membersSet: Set<User> = new Set<User>();
    if (teamController.list.length) {
      teamController.list.forEach((singleController) => {
        singleController.x!.members.forEach((m) => membersSet.add(m));
      });
    }
    setMembers(membersSet);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [teamController.list.length]);
  const selected: MemberCustomOption[] = Array.from(members)
    .filter((m) => selectedMemberUsernames.indexOf(m.username) !== -1)
    .map((m) => ({
      label: m.username,
      value: m.username,
      type: MemberOptionType.MEMBER,
    }));
  if (showTeams) {
    teamController.list.forEach((singleController) => {
      const members = singleController.x!.members;
      const teamItem = {
        label: singleController.x!.name,
        value: singleController.x!.id,
        type: MemberOptionType.TEAM,
        members: members,
      };
      if (selectedTeamIds === undefined) {
        /* When selected teams are not provided separately, teams are
         * selected by selecting all their usernames. */
        const totalSelected: number = members.filter(
          (m) => selectedMemberUsernames.indexOf(m.username) !== -1,
        ).length;
        if (members.length === totalSelected) {
          selected.push(teamItem);
        }
      } else {
        if (selectedTeamIds.indexOf(teamItem.value) !== -1) {
          selected.push(teamItem);
        }
      }
    });
  }
  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
  return (
    <Select
      isMulti
      onChange={onChange}
      options={groupedOptions}
      className={className}
      styles={dropdownSelectStyles}
      isDisabled={isLoading}
      components={{
        IndicatorSeparator: undefined,
        ClearIndicator: undefined,
        Option: SelectCheckboxDropdownItemOption,
        DropdownIndicator: SelectToggleDropdownIndicator({
          label: showTeams ? t("common.team") : t("common.member"),
          countFunc: selectedMemberCountFunc,
        }),
        SelectContainer: SelectDropdownContainer({
          className: "filter scroll scrollbar light-scrollbar",
          onDropdownToggle: (nextValue: boolean) => {
            setIsMenuOpen(nextValue);
          },
        }),
        Menu: SelectDropdownMenu,
        MenuList: SelectDropdownMenuList({ hasSearchBar: true }),
      }}
      value={selected}
      isLoading={teamController.fetching}
      hideSelectedOptions={false}
      closeMenuOnSelect={false}
      closeMenuOnScroll={true}
      isSearchable={true}
      tabSelectsValue={false}
      backspaceRemovesValue={false}
      blurInputOnSelect={false}
      controlShouldRenderValue={false}
      menuIsOpen={isMenuOpen}
    />
  );
};
