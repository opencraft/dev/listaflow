"""Widgets for the form fields."""

import json

from django.forms.widgets import Widget
from django.template import loader
from django.utils.safestring import mark_safe


class SchemaEditorWidget(Widget):
    """Schema editor widget for presenting forms for JSONField."""

    template_name = "schema_editor.html"

    def __init__(self, schema_selector_field_name, schemas, default_values):
        self.schema_selector_field_name = schema_selector_field_name
        self.schemas = schemas
        self.default_values = default_values
        super().__init__()

    def get_context(self, name, value, attrs=None):
        return {
            "id": attrs.get("id"),
            "name": name,
            "context": json.dumps(
                {
                    "id": attrs.get("id"),
                    "schemas": self.schemas,
                    "schema_selector_field_name": self.schema_selector_field_name,
                    "startval": value,
                    "default_values": self.default_values,
                }
            ),
        }

    def render(self, name, value, attrs=None, renderer=None):
        context = self.get_context(name, value, attrs)
        template = loader.get_template(self.template_name).render(context)
        return mark_safe(template)

    class Media:
        js = ("js/jsoneditor.min.js", "js/schema-editor.js")
