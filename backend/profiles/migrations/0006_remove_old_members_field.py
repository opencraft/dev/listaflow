# Generated by Django 3.2.13 on 2022-11-16 23:32
import taggit
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("profiles", "0005_migrate_members"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="team",
            name="members",
        ),
        migrations.RenameField(
            model_name="team",
            old_name="new_members",
            new_name="members",
        ),
        migrations.AlterField(
            model_name="team",
            name="members",
            field=models.ManyToManyField(
                related_name="teams",
                through="profiles.TeamMembership",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AlterField(
            model_name="teammembership",
            name="tags",
            field=taggit.managers.TaggableManager(
                blank=True,
                help_text="A comma-separated list of tags.",
                through="profiles.TaggedMembership",
                to="lib.Tag",
                verbose_name="Tags",
            ),
        ),
    ]
