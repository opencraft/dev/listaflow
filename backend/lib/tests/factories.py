"""
Test model factories for the lib app.
"""

from factory import Faker
from factory.django import DjangoModelFactory

from lib.models import Tag, AuditLog


class TagFactory(DjangoModelFactory):
    """
    Factory for Tag model.
    """

    name = Faker("name")

    class Meta:
        model = Tag


class AuditLogFactory(DjangoModelFactory):
    """
    Factory for AuditLogs
    """

    event_type = Faker("slug")

    class Meta:
        model = AuditLog
