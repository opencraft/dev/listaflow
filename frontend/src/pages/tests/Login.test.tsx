import { useTranslation } from "react-i18next";
import { fireEvent, render, screen, waitFor } from "../../utils/test-utils";
import Login from "../Login";
import { vi, test, expect } from "vitest";

const { t } = useTranslation();

vi.mock("../../constants/api-urls", () => ({
  TOKEN_PATH: "#",
  GOOGLE_CONVERT_TOKEN_PATH: "#",
}));

test("test login form", async () => {
  const { queryAllByText, queryByText } = render(<Login />);

  expect(queryAllByText(/`${t("login.form.title")}`/i)).toBeTruthy();

  fireEvent.change(screen.getByLabelText(t("login.form.email")), {
    target: { value: "e@ecom" },
  });
  fireEvent.blur(screen.getByLabelText(t("login.form.email")));
  await waitFor(() => {
    expect(
      queryByText(/Emails without a full domain name are not supported./i),
    ).toBeTruthy();
  });

  fireEvent.change(screen.getByLabelText(t("login.form.email")), {
    target: { value: "123sadadf23zsdas" },
  });
  fireEvent.change(screen.getByLabelText(t("login.form.email")), {
    target: { value: "e@e.com" },
  });
  await waitFor(() => {
    expect(
      queryByText(/Emails without a full domain name are not supported./i),
    ).toBeNull();
  });
});

test("test google button hidden", async () => {
  const { queryByText } = render(<Login />);
  expect(queryByText(t("loginWithGoogle"))).toBeNull();
});

test("test visible google button", async () => {
  import.meta.env.REACT_APP_GOOGLE_CLIENT_ID = "fake-id";
  const { queryByText } = render(<Login />);

  expect(queryByText(t("loginWithGoogle"))).toBeTruthy();
});
