"""
Django admin module for workflow.
"""

from datetime import timedelta
from logging import ERROR, WARNING

from django import forms
from django.contrib import admin
from django.contrib.messages import SUCCESS
from django.core import mail
from django.db import models, transaction
from django.db.models import Q, Count
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _
from django.forms import Textarea
from django.forms.models import BaseInlineFormSet, InlineForeignKeyField
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin

from lib.admin import LoggingInline
from lib.helpers import log_event
from workflow.helpers.email import send_pending_checklist_email_for_runs
from workflow.interaction import (
    get_interfaces_customization_arg_default_value,
    get_interfaces_customization_arg_schema,
)
from workflow.models import (
    Checklist,
    ChecklistDefinition,
    ChecklistTask,
    ChecklistTaskDefinition,
    Email,
    EventRecurrenceMapping,
    Recurrence,
    Run,
    RunTrendTaskReport,
)

from workflow.widgets import SchemaEditorWidget


class ChecklistTaskDefinitionForm(forms.ModelForm):
    """
    ChecklistTaskDefinition form.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["interface_type"].widget.attrs["class"] = "add-schema-editor"
        existing_classes = self.fields["order"].widget.attrs.get("class", "")
        new_class = "drag-n-drop-enabled"
        self.fields["order"].widget.attrs[
            "class"
        ] = f"{existing_classes} {new_class}".strip()

    class Meta:
        model = ChecklistTaskDefinition

        fields = "__all__"

    class Media:
        js = ("js/sortable.min.js", "js/drag-n-drop.js")


class ChecklistTaskDefinitionInlineBase(admin.TabularInline):
    """
    Base class for add/edit ChecklistTaskDefinition on the same page as parent models
    """

    form = ChecklistTaskDefinitionForm
    model = ChecklistTaskDefinition
    extra = 0
    readonly_fields = ("id_readonly",)
    ordering = ("order",)

    @admin.display(description="ID")
    def id_readonly(self, instance):
        """
        This is a workaround to display empty value for id field in admin UI
        """
        return instance.id if instance.checklist_definition else None

    exclude = (
        "created_on",
        "updated_on",
    )
    # SchemaEditorWidget renders forms based on required fields for the selected
    # interface_type.
    formfield_overrides = {
        models.JSONField: {
            "widget": SchemaEditorWidget(
                "interface_type",
                get_interfaces_customization_arg_schema(),
                get_interfaces_customization_arg_default_value(),
            )
        },
        models.TextField: {"widget": Textarea(attrs={"rows": 4, "cols": 40})},
    }
    show_change_link = True


class ChecklistTaskDefinitionInlineForChecklistFormSet(BaseInlineFormSet):
    """
    Customized FormSet to handle inline tasks creation for checklist definitions
    """

    def __init__(self, *args, **kwargs):
        kwargs["queryset"] = kwargs["queryset"].filter(parent__isnull=True)
        super().__init__(*args, **kwargs)

    def add_fields(self, form, index):
        super().add_fields(form, index)
        form.fields["parent"] = InlineForeignKeyField(None)


class ChecklistTaskDefinitionInlineForChecklist(ChecklistTaskDefinitionInlineBase):
    """
    add/edit ChecklistTaskDefinition on the same page as ChecklistDefinition
    """

    formset = ChecklistTaskDefinitionInlineForChecklistFormSet


class ChecklistTaskDefinitionInlineForTaskFormSet(BaseInlineFormSet):
    """
    Customized FormSet to handle inline tasks creation for parent task definitions
    """

    def add_fields(self, form, index):
        super().add_fields(form, index)
        form.fields["checklist_definition"] = InlineForeignKeyField(
            self.instance.checklist_definition
        )


class ChecklistTaskDefinitionInlineForTask(ChecklistTaskDefinitionInlineBase):
    """
    add/edit ChecklistTaskDefinition on the same page as parent task definition
    """

    formset = ChecklistTaskDefinitionInlineForTaskFormSet


@admin.register(ChecklistDefinition)
class ChecklistDefinitionAdmin(admin.ModelAdmin):
    """
    ChecklistDefinition model admin.
    """

    raw_id_fields = (
        "author",
        "team",
    )
    inlines = [
        ChecklistTaskDefinitionInlineForChecklist,
    ]


@admin.register(ChecklistTaskDefinition)
class ChecklistTaskDefinitionAdmin(admin.ModelAdmin):
    """
    ChecklistTaskDefinition model admin.
    """

    raw_id_fields = ("checklist_definition",)
    list_display = (
        "label",
        "checklist_definition",
        "parent",
    )
    exclude = ("parent",)

    def get_inlines(self, request, obj):
        """
        Dynamically specify inline only if there's no parent task.
        That helps enforce only one level deep of tasks nesting.
        """
        return (
            [
                ChecklistTaskDefinitionInlineForTask,
            ]
            if obj and not obj.parent
            else []
        )


@admin.register(Email)
class EmailModelAdmin(admin.ModelAdmin, DynamicArrayMixin):
    """
    Email model admin.
    """

    list_display = ("subject", "reply_to")


@admin.action(description="Forcibly create next run")
def create_next_run(model_admin, request, queryset):
    """
    Create the next run for a recurrence manually.
    """
    for recurrence in queryset:
        now = timezone.now()
        if not recurrence.start_date:
            start_date = now
        elif (
            last_run := recurrence.runs.filter(end_date__gte=recurrence.start_date)
            .order_by("-end_date")
            .first()
        ):
            # If we're dealing with an active recurrence, respect its current
            # patterning.
            start_date = last_run.end_date
        else:
            start_date = recurrence.start_date
        try:
            with log_event(
                "manual_recurrence_run_creation", targets=[recurrence]
            ), transaction.atomic():
                run = recurrence.create_run(start_date=start_date)
        except Exception as err:  # pylint: disable=broad-exception-caught
            model_admin.message_user(
                request,
                _("Failed creating run for Recurrence ID %(recurrence_id)s: %(err)")
                % {"recurrence_id": recurrence.id, "err": err},
                level=ERROR,
            )
            continue
        if run is None:
            print(
                _(
                    "Skipped Recurrence ID %(recurrence_id)s as no valid "
                    "checklists would be created.",
                )
                % {"recurrence_id": recurrence.id}
            )
            model_admin.message_user(
                request,
                _(
                    "Skipped Recurrence ID %(recurrence_id)s as no valid "
                    "checklists would be created.",
                )
                % {"recurrence_id": recurrence.id},
            )
            continue
        model_admin.message_user(
            request,
            mark_safe(
                _(
                    "Run created for Recurrence ID %(recurrence_id)s with ID"
                    '<a href="%(run_url)s">%(run_id)s</a>'
                )
                % {
                    "recurrence_id": recurrence.id,
                    "run_id": run.id,
                    "run_url": reverse("admin:workflow_run_change", args=[run.id]),
                },
            ),
            level=SUCCESS,
        )


@admin.register(Recurrence)
class RecurrenceAdmin(admin.ModelAdmin, DynamicArrayMixin):
    """
    Recurrence model admin.
    """

    actions = [create_next_run]
    inlines = [LoggingInline]

    list_display = (
        "__str__",
        "active",
        "schedule",
        "start_date",
        "due_days_no",
    )
    exclude = ("periodic_task",)
    raw_id_fields = ("team", "from_user")

    def schedule(self, obj):
        """
        Get the display for whatever kind of schedule this recurrence has.
        """
        if obj.interval_schedule:
            return str(obj.interval_schedule)
        return str(obj.crontab_schedule)

    def get_form(self, request, obj=None, change=False, **kwargs):
        """
        Return a Form class for use in the admin add view. This is used by
        add_view and change_view.
        """
        form = super().get_form(request, obj=None, change=False, **kwargs)

        # remove interval_schedule edit and delete option
        def _disable_edit_delete(field_name):
            field = form.base_fields[field_name]
            field.widget.can_change_related = False
            field.widget.can_delete_related = False

        _disable_edit_delete("interval_schedule")
        _disable_edit_delete("notification_email")
        _disable_edit_delete("reminder_email")
        return form


@admin.action(description="Forcibly send reminder email")
def send_email_reminders(model_admin, request, queryset):
    """
    Forcibly send email reminder for runs.
    """
    # filter out runs with no active checklists
    queryset = queryset.annotate(
        active_checklists_count=Count(
            "checklists", filter=Q(checklists__is_archived=False)
        )
    )

    with mail.get_connection() as conn:
        for run in queryset:
            try:
                # skip runs with no active checklists
                if run.active_checklists_count == 0:
                    with log_event(
                        "manual_send_reminder_emails_no_active_checklists",
                        targets=[run],
                    ):
                        model_admin.message_user(
                            request,
                            _(
                                "Email reminders skipped for %(run)s: "
                                "All the checklists in this Run are archived."
                            )
                            % {"run": run},
                            level=WARNING,
                        )
                    continue

                today = timezone.now().date()
                if today >= run.due_date:
                    days_pending = timedelta(days=0)
                else:
                    days_pending = run.due_date - today
                with log_event("manual_send_reminder_emails", targets=[run]):
                    send_pending_checklist_email_for_runs(run, conn, days_pending)
                model_admin.message_user(
                    request,
                    _("Sent email reminders for %(run)s.") % {"run": run},
                    level=SUCCESS,
                )
            except Exception as err:  # pylint: disable=broad-exception-caught
                model_admin.message_user(
                    request,
                    _(
                        "Failed when sending email reminders for %(run)s. One or more "
                        "team members may not have been emailed. Error was: %(err)s"
                    )
                    % {"run": run, "err": err},
                    level=ERROR,
                )


@admin.register(Run)
class RunAdmin(admin.ModelAdmin, DynamicArrayMixin):
    """
    Recurrence model admin.
    """

    actions = [send_email_reminders]
    inlines = [LoggingInline]

    list_display = (
        "__str__",
        "team",
        "start_date",
        "end_date",
        "due_date",
        "recurrence",
    )
    raw_id_fields = ("team",)


@admin.register(Checklist)
class ChecklistAdmin(admin.ModelAdmin):
    """
    Recurrence model admin.
    """

    list_display = ("__str__", "assignee", "completed", "created_on", "status")
    raw_id_fields = (
        "definition",
        "assignee",
        "run",
        "team",
    )


@admin.register(ChecklistTask)
class ChecklistTaskAdmin(admin.ModelAdmin):
    """
    ChecklistTask model admin
    """

    list_display = (
        "label",
        "assignee",
        "checklist",
        "parent",
    )


@admin.register(RunTrendTaskReport)
class RunTrendTaskReportAdmin(admin.ModelAdmin):
    """
    RunTrendTaskReport model admin.
    """

    list_display = ("task_definition", "run")


@admin.register(EventRecurrenceMapping)
class EventRecurrenceMappingAdmin(admin.ModelAdmin):
    """
    EventRecurrenceMapping model admin.
    """

    list_display = ("id", "name", "recurrence", "notes")
