import { createStore, IModuleStore } from "redux-dynamic-modules";
import { GlobalOptions } from "@opencraft/providence/base/types/GlobalOptions";
import { getList } from "@opencraft/providence-redux/testHelpers";
import { customRender, testContext } from "../../utils/test-utils";
import { Run } from "../../types/Task";
import { RunsTable } from "../RunsTable";
import { expect, vi, describe, it, beforeEach } from "vitest";

vi.mock("../RunsTableRow", () => ({
  RunsTableRow: () => <div data-testid="runs-table-row" />,
}));

let store: IModuleStore<any>;
let context: GlobalOptions;

describe("RunsTable.tsx", () => {
  const run = {
    name: "Test Run",
    is_archived: false,
    assignees: [],
    team_name: "Test Team",
    start_date: "2024-03-07T12:00:00",
    end_date: "2024-03-08T12:00:00",
    due_date: "2024-03-09T12:00:00",
    completed_checklist_count: 5,
    total_checklist_count: 10,
    recurrence: {
      id: "test_recurrence_id",
      periodic_task: 3,
      recurring_schedule_display: "Every 5 minutes",
      team: "test_team_id",
      checklist_definition: "test_checklist_definition_id",
    },
  };
  const runsList = [
    { ...run, id: "test_run_1" },
    { ...run, id: "test_run_2" },
  ];

  beforeEach(() => {
    store = createStore({});
    context = testContext();
  });

  it("Displays one row for each run", async () => {
    const { controller } = getList<Run>(
      ["runs", "active"],
      { endpoint: "#" },
      { store, context },
    );
    await controller.makeReady(runsList);
    const result = customRender(
      <RunsTable controller={controller} onArchiveUnarchive={null} />,
      { store, context },
    );
    expect(await result.findAllByTestId("runs-table-row")).toHaveLength(2);
  });
});
