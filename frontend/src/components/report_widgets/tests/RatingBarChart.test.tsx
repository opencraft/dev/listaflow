import { render, screen } from "../../../utils/test-utils";
import { describe, it, expect } from "vitest";
import RatingBarChart, { RatingBarChartArgs } from "../RatingBarChart";

const dummyRatings = () => ({
  mean: 5.2,
  median: 6,
  mode: 6,
  count: 5,
  buckets: [
    {
      count: 0,
      rating: 1,
    },
    {
      count: 0,
      rating: 2,
    },
    {
      count: 1,
      rating: 3,
    },
    {
      count: 0,
      rating: 4,
    },
    {
      count: 1,
      rating: 5,
    },
    {
      count: 3,
      rating: 6,
    },
    {
      count: 0,
      rating: 7,
    },
    {
      count: 0,
      rating: 8,
    },
    {
      count: 0,
      rating: 9,
    },
    {
      count: 0,
      rating: 10,
    },
  ],
});

const Container = (props: RatingBarChartArgs) => {
  return (
    <div data-testid="beep" style={{ width: "1000px", height: "1000px" }}>
      <RatingBarChart {...props} skipResponsive={true} />
    </div>
  );
};

const sleep = (delay: number) =>
  new Promise((resolve) => setTimeout(resolve, delay));

describe("Bar chart", () => {
  it("Should render a bar chart left to right", async () => {
    window.TEXT_DIRECTION = "ltr";
    render(<Container interfaceStats={dummyRatings()} />);
    expect(
      screen.getByTestId("beep").textContent.startsWith("12345678910"),
    ).toBe(true);
  });
  it("Should render a bar chart right to left", () => {
    window.TEXT_DIRECTION = "rtl";
    render(<Container interfaceStats={dummyRatings()} />);
    expect(
      screen.getByTestId("beep").textContent.startsWith("10987654321"),
    ).toBe(true);
  });
});
