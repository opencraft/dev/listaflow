Confirm your email address

We just want to make sure it's you. Please confirm your email address by clicking the link below.

{{ verification_url | safe }}

Ignore this email if you didn't create a Listaflow account.
