import { Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { MiniAvatar } from "./MiniAvatar";
import { dateString, isFirstOlderThanSecond } from "../utils/helpers";
import { ChecklistAction } from "./ChecklistAction";
import { TaskListReview } from "../types/Task";
import { DisplayUser } from "../types/User";
import { format, parseISO } from "date-fns";
import { REPORTS_COMPARE, USER_CHECKLISTS } from "../constants/urls";
import { useTranslation } from "react-i18next";

declare interface ColSizes {
  xs: number[];
  sm: number[];
  md: number[];
  lg: number[];
}

declare interface RunChecklistTableRowArgs {
  checklist: TaskListReview;
  currentUsername: string;
  colSizes: ColSizes;
}

export const RunChecklistTableRow = ({
  checklist,
  currentUsername,
  colSizes,
}: RunChecklistTableRowArgs) => {
  const isCurrentUserAssignee = checklist.assignee.username === currentUsername;
  const { t } = useTranslation();

  const getTaskReportLink = (taskList: TaskListReview): string => {
    if (!(taskList.run && taskList.run.recurrence)) {
      return "";
    }
    const reportQueryParams = new URLSearchParams({
      checklistDefinition: taskList.run.recurrence.checklist_definition,
      team_ids: taskList.run.recurrence.team,
      start_date: format(parseISO(taskList.run.start_date), "yyyy-MM-dd"),
      end_date: format(parseISO(taskList.run.end_date), "yyyy-MM-dd"),
    });
    return `${REPORTS_COMPARE}?${reportQueryParams.toString()}`;
  };

  const detailLink = `${USER_CHECKLISTS}/${checklist.id}/`;

  const assignee = checklist.assignee as DisplayUser;
  const isPastDue =
    !checklist.completed &&
    isFirstOlderThanSecond(checklist.run?.due_date, new Date());

  const displayName = isCurrentUserAssignee
    ? t("runDetail.checklists.table.row.currentUser", {
        user: assignee.display_name,
      })
    : assignee.display_name;

  return (
    <Row
      key={checklist.id}
      className="item mb-2 rounded py-4 me-0 ms-0 fs-sm run-listing-item"
    >
      <Link to={detailLink} className="link-overlay rounded">
        <span className="d-none">{assignee.username}</span>
      </Link>
      <Col
        xs={colSizes.xs[0]}
        sm={colSizes.sm[0]}
        md={colSizes.md[0]}
        lg={colSizes.lg[0]}
      >
        <div className="d-flex mb-0">
          <MiniAvatar className="me-2" user={assignee} />
          <span title={assignee.username} className="fw-bold text-white">
            {displayName}
          </span>
        </div>
      </Col>
      <Col
        className="d-none d-sm-block"
        xs={colSizes.xs[1]}
        sm={colSizes.sm[1]}
        md={colSizes.md[1]}
        lg={colSizes.lg[1]}
      >
        <span>{assignee.username}</span>
      </Col>
      <Col
        xs={colSizes.xs[2]}
        sm={colSizes.sm[2]}
        md={colSizes.md[2]}
        lg={colSizes.lg[2]}
      >
        <div
          className={`noselect checklist-status ${checklist.status.toLowerCase()}`}
        >
          <small>
            {t(`checklistStatus.${checklist.status.toLowerCase()}`)}
          </small>
        </div>
      </Col>
      <Col
        xs={colSizes.xs[3]}
        sm={colSizes.sm[3]}
        md={colSizes.md[3]}
        lg={colSizes.lg[3]}
      >
        <Row>
          <Col className="d-none d-md-block pe-0" md={10} lg={9}>
            <span className={`${isPastDue ? "text-warning" : ""} noselect`}>
              {dateString(checklist.run?.due_date)}
            </span>
          </Col>
          <Col className="p-0 pe-1" xs={12} md={2} lg={3}>
            <ChecklistAction
              onArchiveUnarchive={null}
              checklist={checklist}
              isOwnedByCurrentUser={isCurrentUserAssignee}
              taskReportLink={getTaskReportLink(checklist)}
              fromMyLists={false}
            />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
