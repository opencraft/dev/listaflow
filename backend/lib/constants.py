"""
Constants for the lib app.
"""

from typing import Union

DEBUG = 0
INFO = 1
WARNING = 2
ERROR = 3

type LogLevel = Union[DEBUG, INFO, WARNING, ERROR]

LOG_LEVEL_CHOICES = (
    (DEBUG, "Debug"),
    (INFO, "Info"),
    (WARNING, "Warning"),
    (ERROR, "Error"),
)
