"""
Database related extensions
"""

from django.db.models import Aggregate, FloatField


# pylint: disable=W0223
class Median(Aggregate):
    """
    Aggregate median value of a queryset
    """

    def __init__(self, expression, **extra):
        super().__init__(expression, **extra)

    function = "PERCENTILE_CONT"
    name = "median"
    output_field = FloatField()
    template = "%(function)s(0.5) WITHIN GROUP (ORDER BY %(expressions)s)"
