import { getSingle } from "@opencraft/providence-redux/testHelpers";
import { createStore, IModuleStore } from "redux-dynamic-modules";
import { defaultContextValues } from "@opencraft/providence-redux/context";
import { GlobalOptions } from "@opencraft/providence/base/types/GlobalOptions";
import { ReadOnlySubsection, Subsection, SubsectionTask } from "../Subsection";
import { render } from "../../utils/test-utils";
import { describe, it, expect, beforeEach } from "vitest";

let store: IModuleStore<any>;
let context: GlobalOptions;

const genSubsection = (): SubsectionTask => ({
  id: "1234",
  customization_args: {},
  label: "Beep",
  body: "This is a test.",
  completed: false,
  completed_on: null,
  required: true,
  parent: null,
  response: null,
  interface_type: "subsection",
});

describe("Subsection.tsx", () => {
  beforeEach(() => {
    store = createStore({});
    context = defaultContextValues();
  });
  it("Renders a subsection.", async () => {
    const subsection = getSingle<SubsectionTask>(
      "subsection",
      {
        endpoint: "/test/",
        x: genSubsection(),
      },
      { store, context },
    ).controller;
    const result = render(
      <Subsection controller={subsection} isCompleted={false}>
        {[<div key={1}>Test 1</div>]}
      </Subsection>,
    );
    expect(result.queryByText("Test 1")).toBeInTheDocument();
    expect(result.queryByText("Beep")).toBeInTheDocument();
    expect(result.queryByText("This is a test.")).toBeInTheDocument();
  });

  it("Renders a read-only subsection.", async () => {
    const subsection = getSingle<SubsectionTask>(
      "subsection",
      {
        endpoint: "/test/",
        x: genSubsection(),
      },
      { store, context },
    ).controller;
    const result = render(
      <ReadOnlySubsection controller={subsection} isCompleted={true}>
        {[<div key={1}>Test 1</div>]}
      </ReadOnlySubsection>,
    );
    expect(result.queryByText("Test 1")).toBeInTheDocument();
    expect(result.queryByText("Beep")).toBeInTheDocument();
    expect(result.queryByText("This is a test.")).not.toBeInTheDocument();
  });
});
