"""
Test the helper functions for the seed_historical_data command.
"""

import pytest
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from django_celery_beat.models import DAYS

from profiles.tests.factories import UserWithNameFactory, TeamWithTaggedUsersFactory
from workflow.interaction import (
    NUMERIC_INTERFACE,
    LINEAR_SCALE_RATING_INTERFACE,
    MULTIPLE_CHOICE_INTERFACE,
    MARKDOWN_TEXTAREA_INTERFACE,
    CHECKBOX_INTERFACE,
    SUBSECTION_INTERFACE,
)
from workflow.management.commands.seed_historical_data import (
    create_historical_run,
    default_range_parameters,
    mock_numeric,
    mock_linear,
    mock_multiple_choice,
    mock_markdown_textarea,
    mock_checkbox,
    random_datetime,
    mock_subsection_interface,
)
from workflow.tests.factories import (
    ChecklistDefinitionWithAllTaskTypes,
    RecurrenceFactory,
    IntervalScheduleFactory,
    ChecklistTaskFactory,
)


@pytest.mark.django_db(transaction=True)
@pytest.mark.freeze_time("2020-05-01")
def test_create_historical_run():
    """
    Verify we can create a historical recurrence-based run with an offset based on a
    specific date.
    """
    last_start_time = timezone.now()
    users = [UserWithNameFactory()]
    team = TeamWithTaggedUsersFactory.create(members=users)
    checklist_definition = ChecklistDefinitionWithAllTaskTypes(author=users[0])
    recurrence = RecurrenceFactory(
        active=False,
        interval_schedule=IntervalScheduleFactory(every=5, period=DAYS),
        checklist_definition=checklist_definition,
        team=team,
    )
    # Factory interval is 5 days.
    new_start_date, historical_run = create_historical_run(last_start_time, recurrence)
    assert new_start_date == last_start_time.replace(month=4, day=26)
    assert historical_run.due_date == last_start_time.date()
    assert historical_run.start_date == new_start_date


@pytest.mark.parametrize(
    "input_min_value,input_max_value,step,sentiment,"
    "chaos,output_min_value,output_max_value",
    [
        (None, 10, 1, 90, 20, 5, 10),
        (0, 10, 1, 90, 20, 7, 10),
        (10, None, 1, 90, 20, 25, 30),
        (0, 50, 1, 30, 5, 13, 16),
        (1, 10, 1, 90, 0, 9, 9),
    ],
)
def test_default_range_parameters(  # pylint: disable=too-many-arguments, too-many-positional-arguments
    input_min_value,
    input_max_value,
    step,
    sentiment,
    chaos,
    output_min_value,
    output_max_value,
):
    """
    Verify that we can generate sane default minimum and maximum values
    given an initial minimum and maximum (which may be None), as well as
    a sentiment and chaos value. Make sure these conform to the step interval.
    """
    assert default_range_parameters(
        min_value=input_min_value,
        max_value=input_max_value,
        step=step,
        sentiment=sentiment,
        chaos=chaos,
    ) == (output_min_value, output_max_value)


@pytest.mark.django_db(transaction=True)
def test_mock_numeric():
    """
    Verify we can supply useful mock data to a numeric response task.
    """
    task = ChecklistTaskFactory(
        definition__interface_type=NUMERIC_INTERFACE,
        definition__customization_args={"min_value": 0, "max_value": 5, "step": 1},
    )
    mock_numeric(task)
    assert 0 <= task.response["number"] <= 5


@pytest.mark.django_db(transaction=True)
def test_mock_numeric_single_option():
    """
    Verify that if only one option is possible for a numeric task,
    that's what we supply.
    """
    task = ChecklistTaskFactory(
        definition__interface_type=NUMERIC_INTERFACE,
        definition__customization_args={"min_value": 0, "max_value": 5, "step": 1},
    )
    mock_numeric(task, sentiment=100, chaos=0)
    assert task.response["number"] == 5


@pytest.mark.django_db(transaction=True)
def test_mock_linear():
    """
    Verify that linear tasks are provided with proper bogus data.
    """
    task = ChecklistTaskFactory(
        definition__interface_type=LINEAR_SCALE_RATING_INTERFACE,
        definition__customization_args={
            "min_value": 1,
            "max_value": 10,
            "max_value_description": "Best",
            "min_value_description": "Worst",
        },
    )
    mock_linear(task, sentiment=90, chaos=0)
    assert task.response["rating"] == 9


@pytest.mark.django_db(transaction=True)
def test_mock_linear_chaos():
    """
    Verify that mock linear task responses are properly chaotic.
    """
    task = ChecklistTaskFactory(
        definition__interface_type=LINEAR_SCALE_RATING_INTERFACE,
        definition__customization_args={
            "min_value": 0,
            "max_value": 10,
            "max_value_description": "Best",
            "min_value_description": "Worst",
        },
    )
    mock_linear(task, sentiment=90, chaos=20)
    assert 7 <= task.response["rating"] <= 10


@pytest.mark.django_db(transaction=True)
def test_mock_multiple_choice():
    """
    Verify that multiple choice tasks are provided with proper bogus data.
    """
    choices = ["Foo", "Bar", "Baz", "Bat", "Boop", "Beep"]
    task = ChecklistTaskFactory(
        definition__interface_type=MULTIPLE_CHOICE_INTERFACE,
        definition__customization_args={
            "minimum_selected": 2,
            "maximum_selected": 5,
            "available_choices": choices,
        },
    )
    mock_multiple_choice(task)
    assert 2 <= len(task.response["selected_choices"]) <= 5
    assert len(set(task.response["selected_choices"])) == len(
        task.response["selected_choices"]
    )
    for item in task.response["selected_choices"]:
        assert item in choices


@pytest.mark.django_db(transaction=True)
def test_mock_markdown_textarea():
    """
    Verify that markdown textareas are provided with proper bogus data.
    """
    task = ChecklistTaskFactory(
        definition__interface_type=MARKDOWN_TEXTAREA_INTERFACE,
        definition__customization_args={
            "placeholder": "Please enter some stuff...",
        },
    )
    mock_markdown_textarea(task)
    assert len(task.response["content"]) > 0


@pytest.mark.django_db(transaction=True)
def test_mock_checkbox_required():
    """
    Verify that checkboxes are provided with proper bogus data.
    """
    task = ChecklistTaskFactory(
        definition__interface_type=CHECKBOX_INTERFACE,
        definition__required=True,
    )
    mock_checkbox(task, sentiment=20)
    assert task.response["checked"]


@pytest.mark.django_db(transaction=True)
def test_mock_checkbox_full_sentiment():
    """
    Verify that checkboxes are provided with proper bogus data.
    """
    task = ChecklistTaskFactory(
        definition__interface_type=CHECKBOX_INTERFACE,
        definition__required=False,
    )
    mock_checkbox(task, sentiment=100)
    assert task.response["checked"]


@pytest.mark.django_db(transaction=True)
def test_mock_checkbox_no_sentiment():
    """
    Verify that checkboxes are provided with proper bogus data.
    """
    task = ChecklistTaskFactory(
        definition__interface_type=CHECKBOX_INTERFACE,
        definition__required=False,
    )
    mock_checkbox(task, sentiment=0)
    assert not task.response["checked"]


@pytest.mark.django_db(transaction=True)
def test_mock_subsection_interface():
    """
    Verify that subsections are provided with proper bogus data.
    """
    task = ChecklistTaskFactory(
        definition__interface_type=SUBSECTION_INTERFACE,
    )
    mock_subsection_interface(task)
    # Shouldn't actually need to do anything.
    assert task.response is None


def test_random_datetime():
    """
    Verify that random datetimes are created within the specified range.
    """
    start = timezone.now() + relativedelta(days=1)
    end = timezone.now() + relativedelta(days=10)
    result = random_datetime(start, end)
    assert start < result < end
