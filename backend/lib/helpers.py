"""
Decorator functions useful for leveraging lib models.
"""

import traceback
from contextlib import contextmanager
from logging import getLogger
from pprint import pformat
from typing import Optional

from django.db.models import Model
from django.template.defaultfilters import slugify

from lib.constants import LogLevel, INFO, DEBUG, WARNING, ERROR
from lib.models import AuditLog, ref_for_instance


logger = getLogger(__name__)

LOG_FUNCS = {
    DEBUG: logger.debug,
    INFO: logger.info,
    WARNING: logger.warning,
    ERROR: logger.error,
}


@contextmanager
def log_event(
    event_type: str,
    level: LogLevel = INFO,
    fail_level: LogLevel = ERROR,
    data: dict = None,
    targets: Optional[list[Model]] = None,
):
    """
    Context manager that will create a log entry upon success or failure for a specified
    event_type.
    """
    data = data or {}
    if (
        (not event_type)
        or (slugify(event_type) != event_type)
        or (len(event_type) > 50)
    ):
        # We could coerce into a slug, but that will cause problems later when
        # searching logs, as looking for the event type as written in the code will
        # yield nothing.
        raise ValueError(f"{repr(event_type)} is not a proper slug.")
    message = AuditLog(event_type=event_type, level=level, data=data)
    try:
        yield message
    except Exception as err:
        message.level = fail_level
        message.data["exception"] = f"{err.__class__.__name__}: {err}"
        message.data["traceback"] = traceback.format_exc()
        raise err
    finally:
        # This should be able to run even if we can't save to DB for some reason (such
        # as a transaction rollback). This means the most authoritative list of log
        # messages will be the console.
        LOG_FUNCS[message.level](
            "%s AuditLog: %s for %s with data %s",
            message.created_on,
            message.event_type,
            targets or [],
            pformat(message.data),
        )
        message.save()
        targets = [ref_for_instance(target) for target in (targets or [])]
        message.targets.add(*targets)
