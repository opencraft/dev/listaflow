import { useTranslation } from "react-i18next";
import CheckIcon from "../../assets/icons/check.svg?react";
import { InterfaceComponentProps } from "../../types/Interaction";

export const Checkbox = ({ response }: InterfaceComponentProps) => {
  const { t } = useTranslation();
  const checked = response.model?.checked;

  return (
    <div className={"checkbox " + (checked ? "checked" : "")}>
      {checked ? <CheckIcon className="base-icon sm fill-dark" /> : <></>}
      <span>{t(checked ? "taskItem.checked" : "taskItem.notChecked")}</span>
    </div>
  );
};
