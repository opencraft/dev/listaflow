"""
Useful snippets/abstract models for the project.
"""

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

# Create your models here.
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from short_stuff import unslugify, gen_shortcode
from short_stuff.django.models import ShortCodeField
from taggit.models import TagBase, ItemBase

from lib.constants import LOG_LEVEL_CHOICES, INFO


class AutoDateTimeField(models.DateTimeField):
    """
    Field for automatically setting the time on a field when it's updated.
    This is preferable to auto_now, which has several subtle bugs involving
    timezones.
    """

    def pre_save(self, model_instance, add):
        return timezone.now()


class DateTrackedModel(models.Model):
    """
    Used on models that will need to be tracked on creation and update.
    """

    created_on = models.DateTimeField(db_index=True, default=timezone.now)
    updated_on = AutoDateTimeField(db_index=True, default=timezone.now)

    class Meta:
        abstract = True


class OrderedModel(models.Model):
    """
    Used on models that allow for user defined ordering functionality
    """

    order = models.PositiveIntegerField(
        help_text="Ascending order placement of this model instance",
        db_index=True,
    )

    class Meta:
        abstract = True
        ordering = ("order",)


class Tag(TagBase):
    """
    Custom tag class. As yet, we have no need for customization on the tag class
    itself, however, we're electing to use custom tag through tables, and if we're
    going that far, we may as well make a custom tag model to make any future needed
    changes not require massive migrations if we need it later.
    """

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")


class TaggedItemBase(ItemBase):
    """
    Same here as with Tag-- basically copy-pasted from upstream per django-taggit's
    docs.
    """

    tag = models.ForeignKey(
        Tag, related_name="%(app_label)s_%(class)s_items", on_delete=models.CASCADE
    )

    class Meta:
        abstract = True


class GenericReference(models.Model):
    """
    Generic reference to an object suitable for insertion in a ManyToMany leaf table.
    """

    # Note: UUIDfields have a large enough bitwidth that they can encompass all integer
    # values we could realistically encounter from models using integer primary keys.
    # Setting an integer to this field 'just works', translating it to its bit value in
    # UUID form.
    id = ShortCodeField(default=gen_shortcode, primary_key=True)
    object_id = models.UUIDField(db_index=True)
    content_type = models.ForeignKey(
        ContentType, on_delete=models.SET_NULL, null=True, blank=False
    )
    target = GenericForeignKey("content_type", "object_id")

    def __str__(self):
        return f"::ref#{self.id}:: {self.target}"

    class Meta:
        unique_together = (("object_id", "content_type"),)


def ref_for_instance(instance: models.Model) -> GenericReference:
    """
    Gets the GenericRef for a particular instance.
    """
    if isinstance(instance, GenericReference):
        return instance
    pk = instance.pk
    if isinstance(pk, str):
        pk = unslugify(pk)
    result, _created = GenericReference.objects.get_or_create(
        content_type=ContentType.objects.get_for_model(instance),
        object_id=pk,
    )
    return result


class AuditLog(models.Model):
    """
    Used for logging events that may be of use for diagnosing issues.

    TODO: Mirror to standard logging.
    """

    id = ShortCodeField(default=gen_shortcode, primary_key=True)
    event_type = models.SlugField(db_index=True, max_length=50)
    level = models.IntegerField(db_index=True, default=INFO, choices=LOG_LEVEL_CHOICES)
    created_on = models.DateTimeField(db_index=True, default=timezone.now)
    data = models.JSONField(default=dict)
    targets = models.ManyToManyField(GenericReference, related_name="audit_logs")
