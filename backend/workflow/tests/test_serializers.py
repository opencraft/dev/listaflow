"""
Test for workflow serializer
"""

import pytest

from workflow.serializers import RunOverviewSerializer
from workflow.tests.utils import create_team_and_checklist_definition
from workflow.tests.factories import RecurrenceFactory


pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("celery_worker", "patch_flush_many_cache"),
]


def test_run_overview_serializer_assignee():
    """Test run overview serializer"""
    (
        team,
        checklist_definition,
        _,
    ) = create_team_and_checklist_definition()
    recurrence_model = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    run = recurrence_model.create_run()
    serializer = RunOverviewSerializer(
        instance=run, context={"users": team.members.all()}
    )
    data = serializer.data
    assignees = data["assignees"]
    for assignee in assignees:
        assert "im_handle" in assignee.keys()
