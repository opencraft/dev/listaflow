"""
Command to replace the current database with a dump.
"""

from pathlib import Path
from typing import Dict, List

from django.conf import settings
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError, CommandParser
from django.db import connection


def replace_db():
    """
    Perform the actual DB replacement, loading the replacement DB dump from stdin.
    """
    script_path = str(
        Path(settings.BASE_DIR) / "lib" / "management" / "commands" / "sql" / "drop.sql"
    )
    with open(script_path, "r", encoding="utf-8") as script_file:
        script = script_file.read()
    with connection.cursor() as cursor:
        cursor.execute(script)
    call_command("dbshell")


class Command(BaseCommand):
    """
    Command to replace the current database with a dump.
    """

    help = "Clears DB and replaces it with stdin (useful for restoring DB dumps)"

    def add_arguments(self, parser: CommandParser):
        """
        Add arguments to the command parser.
        """
        parser.add_argument(
            "--force",
            required=False,
            default=False,
            action="store_true",
            help="Don't check if we're running in a production environment.",
        )

    def handle(self, *args: List, **options: Dict):
        """
        Main command action function.
        """
        if not (options["force"] or settings.ENV_NAME == "Development"):
            raise CommandError(
                "You cannot run this in non-development environments without the"
                "--force flag. It could ruin everything!"
            )
        replace_db()
