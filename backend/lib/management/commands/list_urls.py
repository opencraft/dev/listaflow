"""
URL listing utility command.

Adapted/improved from the simpler example at
https://www.geeksforgeeks.org/how-can-i-list-urlpatterns-endpoints-on-django/
"""

from typing import Union

from django.core.management.base import BaseCommand, OutputWrapper
from django.urls import get_resolver, URLPattern, URLResolver


def enumerate_paths(
    url_patterns: list[Union[URLPattern, URLResolver]],
    level: int,
    stdout: OutputWrapper,
):
    """
    Recursively enumerate all URL paths in a URL patterns list.
    """
    for pattern in url_patterns:
        stdout.write(("  " * level) + str(pattern))
        if hasattr(pattern, "url_patterns"):
            enumerate_paths(pattern.url_patterns, level + 1, stdout)


class Command(BaseCommand):
    """
    Lists all URL patterns in the application.
    """

    help = "List all URL patterns in the project"

    def handle(self, *args, **kwargs):
        self.stdout.write("List of URL patterns:")
        enumerate_paths(get_resolver().url_patterns, 0, self.stdout)
