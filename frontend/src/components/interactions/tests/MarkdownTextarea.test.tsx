import {
  fireEvent,
  render,
  screen,
  testContext,
} from "../../../utils/test-utils";
import { vi, beforeEach, describe, it, expect } from "vitest";
import { MarkdownTextarea } from "../MarkdownTextarea";
import { InterfaceComponentProps } from "../../../types/Interaction";
import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";
import { useTranslation } from "react-i18next";
import { createStore, IModuleStore } from "redux-dynamic-modules";
import { GlobalOptions } from "@opencraft/providence/base/types/GlobalOptions";
import { getSingle } from "@opencraft/providence-redux/testHelpers";

let store: IModuleStore<any>;
let context: GlobalOptions;
let controller: SingleController<InterfaceComponentProps>;
const { t } = useTranslation();

vi.mock("react-markdown", () => {
  return {
    default: () => {
      return <p>Markdown preview</p>;
    },
  };
});

describe("MarkdownTextarea component", () => {
  beforeEach(async () => {
    store = createStore({});
    context = testContext();
    controller = getSingle<InterfaceComponentProps>(
      "testController",
      {
        endpoint: "#",
        x: {
          customizationArgs: { placeholder: "Type your text here..." },
          response: null,
        },
      },
      { store, context },
    ).controller;
    render(
      <MarkdownTextarea
        customizationArgs={controller.x!.customizationArgs}
        response={controller.p.response}
      />,
      { context, store },
    );
  });

  it("should render textarea correctly", async () => {
    const textarea = screen.getByPlaceholderText("Type your text here...");
    expect(textarea).toBeInTheDocument();
  });

  it("should update response correctly on updating the textarea value", async () => {
    const textarea = screen.getByPlaceholderText("Type your text here...");
    fireEvent.change(textarea, {
      target: { value: "Some text" },
    });

    expect(controller.p.response.model).toStrictEqual({
      content: "Some text",
    });
  });

  it("should correctly highlight active tab", async () => {
    const writeTabButton = screen.getByText(t("interactions.write"));
    const previewTabButton = screen.getByText(t("interactions.preview"));

    expect(writeTabButton.className).toContain("markdown-editor-active-tab");
    expect(previewTabButton.className).not.toContain(
      "markdown-editor-active-tab",
    );

    fireEvent.click(previewTabButton);
    expect(writeTabButton.className).not.toContain(
      "markdown-editor-active-tab",
    );
    expect(previewTabButton.className).toContain("markdown-editor-active-tab");
  });

  it("should remove textarea when preview tab is clicked", async () => {
    const writeTabButton = screen.getByText(t("interactions.write"));
    const previewTabButton = screen.getByText(t("interactions.preview"));

    fireEvent.click(writeTabButton);
    let textarea = screen.getByPlaceholderText("Type your text here...");
    expect(textarea).toBeInTheDocument();

    fireEvent.click(previewTabButton);
    expect(textarea).not.toBeInTheDocument();
  });

  it("should remove markdown when write tab is clicked", async () => {
    const writeTabButton = screen.getByText(t("interactions.write"));
    const previewTabButton = screen.getByText("interactions.preview");

    fireEvent.click(previewTabButton);
    const markdownPreview = screen.getByText("Markdown preview");
    expect(markdownPreview).toBeInTheDocument();

    fireEvent.click(writeTabButton);
    expect(markdownPreview).not.toBeInTheDocument();
  });
});
