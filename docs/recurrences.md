# Recurrences

Recurrences are used for the repeating checklist functionality in Listaflow. They have several features to make them useful for teams that need to complete checklists on a regular interval.

Recurrences can be configured in the [admin panel](http://localhost:8000/admin/workflow/recurrence/).

A bundle of checklists created together via recurrence is known as a **'checklist run'** or just a **'run'** for short.

## Scheduling

Recurrences can be set up to create checklist runs on either a periodic interval, or a [crontab](https://www.codementor.io/@akul08/the-ultimate-crontab-cheatsheet-5op0f7o4r)-style schedule (but not both). All recurrences must have a schedule set, even if not active.

A checklist can be scheduled by multiple recurrences. This might be necessary, for instance, if several teams use the same checklist, or the schedule for a checklist is too complex to be represented by one crontab or one interval.

### Interval scheduling

Interval scheduling allows you to schedule recurring checklists for a team on a recurring interval which cannot be expressed in [crontab](#crontab-scheduling) format. Some intervals can also be expressed in crontab style, but may be easier to understand as an interval.

Some example intervals include:

* Every day (expressible via crontab)
* Every 2 weeks (not expressible via crontab)
* Every 5 days (not expressible via crontab)

### Crontab scheduling

Crontab scheduling follows a format similar to [UNIX cron](https://www.codementor.io/@akul08/the-ultimate-crontab-cheatsheet-5op0f7o4r)'s crontab format. Crontab's format is useful when a recurrence's period is long or isn't best/possibly expressed via [interval scheduling](#interval-scheduling).

Some example crontab intervals include:

* Every day at midnight `0 0 * * *` (expressible via interval scheduling)
* Every month on the first day at midnight `0 0 0 * *` (not expressible via interval scheduling, believe it or not)
* Every month whose number is divisible by four on the first day at midnight `0 0 0 0 */4` (not expressible via interval scheduling. Note that this doesn't mean "every four months")

### A word of caution for crontabs

Because crontab scheduling allows you to pattern match times, rather than verify a specific amount of time has passed, there are some edge cases to consider:

* A task scheduled to run on the 29th of every month will fail to run in February 3 out of every 4 years([ish](https://en.wikipedia.org/wiki/Leap_year))
* A task that runs close to a daylight savings transition time may run twice by mistake, or not at all, depending.

## Tags

Recurrences may filter a team by tags. Adding tags in the Recurrence's tags field will require that **all** tags added must be present in order for a team member to be added to the run. Tags may be set on an individual team member's membership in the [teams panel](http://localhost:8000/admin/profiles/team/) in the admin.

## Email Reminders

Listaflow can email reminders to participants who haven't yet completed their checklist. Multiple reminders can be specified-- each by the number of days prior to the due date that a reminder should be sent. Listaflow comes out of the box with beautiful reminder templates. Two different templates are supported-- one for reminders before the due date, and one for reminders on the due date.

You can override these templates as needed for your team, and you can set a person whose name and email will be used as the 'from user' for these reminder emails, so they appear to come directly from a team member.

Email overrides are interpreted as Django templates, and have access to the following variables:

| Name           | Type                 | Description                                                                                                                           |
|----------------|----------------------|---------------------------------------------------------------------------------------------------------------------------------------|
| display_name   | str                  | The display name of the user we're addressing. That could be their name or their username, depending on how configured their user is. |
| checklist_name | str                  | The name of the checklist that they need to complete.                                                                                 |
| checklist_id   | str                  | cell 9                                                                                                                                |
| task_map | dict[str, list[str]] | A mapping of all top-level task labels to the labels of tasks underneath them. (That is, subsection labels to task names) |
| from_name | str | The display name of the 'from user' on the Recurrence's settings. |
| from_user | User | The user object of the 'from user' on the Recurrence's settings. |
