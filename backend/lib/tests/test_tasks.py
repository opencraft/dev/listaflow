"""
Tests for lib celery tasks.
"""

from unittest.mock import patch

import pytest
from dateutil.relativedelta import relativedelta
from django.test import override_settings
from django.utils import timezone
from requests import HTTPError

from lib.models import AuditLog
from lib.tasks import snitch_check_in, remove_old_logs
from lib.tests.factories import AuditLogFactory

pytestmark = [
    pytest.mark.django_db(transaction=True),
]


@override_settings(DEAD_MANS_SNITCH_URL="https://example.com/")
@patch("lib.tasks.requests.get")
def test_snitch_called(mock_get):
    """
    Test that setting a Dead Man's Snitch endpoint causes the endpoint to be called.
    """
    snitch_check_in()
    mock_get.assert_called_with("https://example.com/", timeout=5)


@override_settings(DEAD_MANS_SNITCH_URL="")
@patch("lib.tasks.requests.get")
def test_snitch_not_called(mock_get):
    """
    Test that having a blank setting for Dead Man's Snitch skips snitching.
    """
    snitch_check_in()
    mock_get.assert_not_called()


@override_settings(DEAD_MANS_SNITCH_URL="https://example.com/")
def test_raises_on_error(requests_mock):
    """
    Test that an HTTP error raises properly so we know about it in the logs.
    """
    requests_mock.get(
        "https://example.com/", status_code=404, json={"detail": "Not Found"}
    )
    with pytest.raises(HTTPError):
        snitch_check_in()


@override_settings(AUDIT_LOG_PRESERVATION_DAYS=5)
def test_remove_old_logs():
    """
    Test that the log clearing function only removes logs past the cutoff.
    """
    new_log = AuditLogFactory.create()
    old_log = AuditLogFactory.create(created_on=timezone.now() - relativedelta(days=2))
    old_enough_log = AuditLogFactory.create(
        created_on=timezone.now() - relativedelta(days=6)
    )
    remove_old_logs()
    # Should not raise.
    new_log.refresh_from_db()
    # Also should not raise.
    old_log.refresh_from_db()
    # But this should.
    with pytest.raises(AuditLog.DoesNotExist):
        old_enough_log.refresh_from_db()
