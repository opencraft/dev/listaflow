import { JSXElementConstructor, ReactElement } from "react";
import { ResponsiveContainer } from "recharts";

export const ChartContainer = ({
  children,
  skip,
}: {
  // Can't use ReactNode here because recharts doesn't.
  children: ReactElement<any, string | JSXElementConstructor<any>>;
  skip: boolean;
}) => {
  if (skip) {
    return <>{children}</>;
  }
  return (
    <ResponsiveContainer width="100%" height="100%">
      {children}
    </ResponsiveContainer>
  );
};
