import { act } from "@testing-library/react";
import { Run } from "../../types/Task";
import { RunAction } from "../RunAction";
import { expect, vi, describe, it } from "vitest";
import { customRender, mockAxios } from "../../utils/test-utils";

describe("RunAction.tsx", () => {
  const user1 = {
    id: 1,
    username: "testuser1",
    email: "test1@example.com",
  };
  const run = {
    id: "test_run_id",
    start_date: "2024-03-07T12:00:00Z",
    end_date: "2024-03-08T12:00:00Z",
    due_date: "2024-03-07",
    name: "Test Run",
    team_name: "Test Team",
    is_archived: false,
    completed_checklist_count: 0,
    total_checklist_count: 1,
    assignees: [
      {
        ...user1,
        completed: false,
        display_name: "Test User 1",
      },
    ],
    recurrence: {
      id: "test_recurrence_id",
      periodic_task: 3,
      recurring_schedule_display: "Every 5 minutes",
      team: "test_team_id",
      checklist_definition: "test_checklist_definition_id",
    },
  };

  it("Shows archive option on unarchived runs", async () => {
    const result = customRender(
      <RunAction
        onArchiveUnarchive={null}
        run={run}
        taskReportLink="/task_report_test"
      />,
    );
    act(() => result.getByRole("button").click());
    await result.findByText("runs.action.archive");
  });

  it("Shows unarchive option on archived runs", async () => {
    const result = customRender(
      <RunAction
        onArchiveUnarchive={null}
        run={{ ...run, is_archived: true }}
        taskReportLink="/task_report_test"
      />,
    );
    act(() => result.getByRole("button").click());
    await result.findByText("runs.action.unarchive");
  });

  it("Calls the archive-unarchive function", async () => {
    const onArchiveUnarchive = vi.fn();
    const result = customRender(
      <RunAction
        onArchiveUnarchive={onArchiveUnarchive}
        run={run}
        taskReportLink="/task_report_test"
      />,
    );
    act(() => result.getByRole("button").click());
    const button = await result.findByText("runs.action.archive");
    act(() => button.click());
    expect(onArchiveUnarchive).toHaveBeenCalled();
  });
});
