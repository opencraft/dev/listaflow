export declare interface Message {
  message: string;
  variant?: string;
}
