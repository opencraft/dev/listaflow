import { useTranslation } from "react-i18next";
import { InterfaceComponentProps } from "../../types/Interaction";

export const MultipleChoice = ({ response }: InterfaceComponentProps) => {
  const { t } = useTranslation();
  const selectedChoices = response.model?.selected_choices || [];

  return selectedChoices.length == 0 ? (
    <span>{t("taskItem.noAnswer")}</span>
  ) : (
    <ul className="ps-3">
      {selectedChoices.map((choice: string) => (
        <li>{choice}</li>
      ))}
    </ul>
  );
};
