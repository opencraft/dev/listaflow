"""
Test the schema generation library works as expected.
"""

import json
from io import StringIO

from django.core.management import call_command


def test_schema_generation():
    """
    This test will fail if any of the views we've created aren't auto-describable by
    DRF-YASG.
    """
    output = StringIO()
    call_command("generate_swagger", stdout=output)
    schema = json.loads(output.getvalue())
    assert schema["info"]["title"] == "Listaflow API"
