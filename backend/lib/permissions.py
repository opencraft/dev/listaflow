"""
Permission classes that could be useful anywhere.
"""

from typing import Any

from django.utils.translation import gettext_lazy as _
from rest_framework.permissions import BasePermission
from rest_framework.request import Request
from rest_framework.views import APIView


def is_safe(request: Request) -> bool:
    """
    Checks if a request method is safe.
    """
    return request.method in ["GET", "HEAD", "OPTIONS"]


class IsSafeMethod(BasePermission):
    """
    Checks if a request is using a 'safe' method according to HTTP convention.
    """

    message = _("You are not permitted to modify this.")

    def has_permission(self, request: Request, view: APIView) -> bool:
        """
        View-wide check.
        """
        return is_safe(request)

    def has_object_permission(self, request: Request, view: APIView, obj: Any) -> bool:
        """
        Makes sure this check isn't skipped when applying to a specific object.
        """
        return is_safe(request)


class IsSuperuser(BasePermission):
    """
    Checks if the requesting user is a superuser.
    """

    def has_permission(self, request, view):
        """
        Performs a view-wide check
        """
        return request.user.is_superuser

    def has_object_permission(self, request, view, obj):
        """
        Makes sure this check isn't skipped when applying to a specific object.
        """
        return request.user.is_superuser


def IsAction(*args: str):  # pylint: disable=invalid-name
    """
    Class factory that produces a permission class which checks if the relevant
    ViewSet is being invoked with one of the provided named actions.
    """

    class WrappedPermission(BasePermission):
        """
        Generated wrapped permission class with the given action names.
        """

        message = _("You don't have permission to perform that action.")

        def has_permission(self, request, view):
            """
            Performs view-wide check
            """
            return request.action in args

        def has_object_permission(self, request, view, obj):
            """
            Makes sure this check isn't skipped when applying to a specific object.
            """
            return self.has_permission(request, view)

    return WrappedPermission
