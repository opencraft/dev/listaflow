import { NumericInput } from "../NumericInput";
import { render } from "../../../utils/test-utils";
import { describe, it, expect } from "vitest";

describe("NumericInput.tsx", () => {
  it("Renders a numeric response.", () => {
    const result = render(
      <NumericInput
        customizationArgs={null}
        response={{ model: { number: 3 } }}
      />,
    );
    expect(result.queryByText("3")).toBeInTheDocument();
    expect(result.queryByText("taskItem.noAnswer")).not.toBeInTheDocument();
  });

  it("Renders an empty response.", () => {
    const result = render(
      <NumericInput customizationArgs={null} response={{ model: {} }} />,
    );
    expect(result.queryByText("taskItem.noAnswer")).toBeInTheDocument();
  });
});
