import { fireEvent } from "@testing-library/dom";
import { ChecklistDetail } from "../ChecklistDetail";
import { GlobalOptions } from "@opencraft/providence/base/types/GlobalOptions";
import ErrorTracking from "@opencraft/providence/base/types/ErrorTracking";
import { createStore, IModuleStore } from "redux-dynamic-modules";
import { TaskList } from "../../types/Task";
import { ListController } from "@opencraft/providence/base/lists/types/ListController";
import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";
import { Task, ChecklistStatus } from "../../types/Task";
import { getSingle, getList } from "@opencraft/providence-redux/testHelpers";
import { waitFor } from "@testing-library/react";
import { beforeEach, expect, describe, it, vi } from "vitest";
import {
  customRender,
  flushPromises,
  genRun,
  genUser,
  mockAxios,
  testContext,
} from "../../utils/test-utils";

let store: IModuleStore<any>;
let context: GlobalOptions;
let mockErrors: ErrorTracking = {
  messages: ["this is an error"],
  status: "400",
};
let checklistController: SingleController<TaskList>;
let tasksController: ListController<Task>;
let initialEntries: string[];
let paths: string[];

const { mockUseLocation } = vi.hoisted(() => {
  return { mockUseLocation: vi.fn() };
});

vi.mock("react-router-dom", async (importOriginal) => {
  return {
    ...(await importOriginal<typeof import("react-router-dom")>()),
    useLocation: mockUseLocation,
  };
});

describe("ChecklistDetail page", () => {
  const genTasks: () => Task[] = () => [
    {
      id: "1",
      label: "delectus aut autem",
      body: "body 1",
      interface_type: "checkbox",
      required: true,
      parent: null,
      completed: false,
      completed_on: null,
      response: {},
      customization_args: {},
    },
    {
      id: "2",
      label: "quis ut nam facilis et officia qui",
      body: "body 2",
      interface_type: "checkbox",
      required: true,
      parent: null,
      completed: false,
      completed_on: null,
      response: {},
      customization_args: {},
    },
    {
      id: "3",
      label: "subsection",
      body: "",
      interface_type: "subsection",
      required: false,
      parent: null,
      completed: false,
      completed_on: null,
      response: {},
      customization_args: {},
    },
    {
      id: "4",
      label: "subtask",
      interface_type: "checkbox",
      required: false,
      body: "",
      parent: "3",
      completed: false,
      completed_on: null,
      response: {},
      customization_args: {},
    },
  ];

  const checklistData = {
    id: "beep",
    name: "some name",
    body: "some body",
    tasks: genTasks(),
    definition: "",
    assignee: genUser(),
    created_on: new Date(),
    completed: false,
    completed_on: null,
    force_complete: false,
    status: ChecklistStatus.TO_DO,
    run: genRun(),
  };

  beforeEach(async () => {
    store = createStore({});
    context = testContext();
    const username = "fox";
    const params = { checklistId: "beep" };
    const { checklistId } = params;
    initialEntries = ["/checklist/beep/"];
    paths = ["/checklist/:checklistId"];
    checklistController = getSingle<TaskList>(
      [username, checklistId],
      {
        endpoint: `#checklist`,
      },
      { store, context },
    ).controller;

    checklistController.makeReady(checklistData);

    tasksController = getList<Task>(
      [checklistId, "task"],
      {
        endpoint: "#task",
      },
      { store, context },
    ).controller;

    tasksController.makeReady(genTasks());
    await flushPromises();

    const submissionCount = getSingle<{ num: number }>(
      [checklistId!, "submissionCount"],
      {
        endpoint: "#submissionCount",
        x: { num: 1 },
      },
      { context, store },
    ).controller;
  });

  it("renders providence list items", async () => {
    const result = customRender(<ChecklistDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });

    const checklist1 = result.getByText(/delectus aut autem/i);
    expect(checklist1).toBeInTheDocument();

    const checklist2 = result.getByText(/quis ut nam facilis et officia qui/i);
    expect(checklist2).toBeInTheDocument();
  });

  it("renders providence errors", async () => {
    checklistController.ready = false;
    mockAxios.onGet("#checklist").reply(403, { detail: ["This is an error"] });
    const ui = <ChecklistDetail />;
    const result = customRender(ui, { context, store, initialEntries, paths });
    const checklist1 = await waitFor(() =>
      result.getByText(/This is an error/i),
    );
    expect(checklist1).toBeInTheDocument();
  });

  it("renders subsections expanded", async () => {
    const result = customRender(<ChecklistDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });
    expect(result.getByText("subsection")).toBeInTheDocument();
    const subtask = result.getByText("subtask");
    expect(subtask).toBeInTheDocument();
    const subtaskAccordionCollapse = subtask.closest(".accordion-collapse");
    expect(subtaskAccordionCollapse).not.toBeNull();
    expect(subtaskAccordionCollapse!.className).toBe(
      "accordion-collapse collapse show",
    );
  });

  it("renders pending notice", async () => {
    tasksController.makeReady([
      {
        id: "4",
        label: "subtask",
        body: "",
        interface_type: "checkbox",
        parent: "3",
        required: true,
        completed: false,
        completed_on: null,
        response: {},
        customization_args: {},
      },
    ]);

    const result = customRender(<ChecklistDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });

    const pendingTaskNotice = result.getByText(
      /checklistDetail.form.pendingTasks/i,
    );
    expect(pendingTaskNotice).toBeInTheDocument();
  });

  it("Sends the user to the incomplete required item", async () => {
    const mockScroll = vi.fn();
    window.HTMLElement.prototype.scrollIntoView = mockScroll;
    tasksController.makeReady([
      {
        id: "4",
        label: "task",
        body: "",
        interface_type: "checkbox",
        required: true,
        parent: null,
        completed: false,
        completed_on: null,
        response: {},
        customization_args: {},
      },
    ]);

    const result = customRender(<ChecklistDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });
    expect(mockScroll).not.toHaveBeenCalled();
    const btn = result.getByText(/checklistDetail.form.submitBtn/i);
    fireEvent.click(btn);
    await waitFor(() =>
      result.getByText(/checklistDetail.form.pleaseComplete/i),
    );
    expect(mockScroll).toHaveBeenCalled();
  });

  it("renders force submit alert on submit", async () => {
    tasksController.makeReady([
      {
        id: "4",
        label: "subtask",
        body: "",
        interface_type: "checkbox",
        parent: "3",
        required: true,
        completed: false,
        completed_on: null,
        response: {},
        customization_args: {},
      },
    ]);
    const result = customRender(<ChecklistDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });

    const btn = result.getByText(/checklistDetail.form.submitBtn/i);
    expect(btn).toBeInTheDocument();
    const pendingTaskNotice = result.getByText(
      /checklistDetail.form.pendingTasks/i,
    );
    expect(pendingTaskNotice).toBeInTheDocument();
    fireEvent.click(btn);
    // shows alert and hides btn and other text around it.
    const alert = result.getByText(/checklistDetail.form.forceSaveAlertTitle/i);
    expect(alert).toBeInTheDocument();
    expect(btn).not.toBeInTheDocument();
    expect(pendingTaskNotice).not.toBeInTheDocument();
  });

  it("renders read only checklist", () => {
    checklistController.makeReady({
      ...checklistData,
      assignee: genUser({ username: "user2" }),
      completed: true,
    });

    const result = customRender(<ChecklistDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });

    expect(result.queryByText(/delectus aut autem/i)).toBeInTheDocument();
    // Read-only unchecked checkbox text.
    expect(result.getAllByText("taskItem.notChecked")[0]).toBeInTheDocument();
    expect(
      result.queryByText("checklistDetail.sendReminder"),
    ).not.toBeInTheDocument();
  });

  it("renders reminder button on incomplete read only checklist", () => {
    checklistController.makeReady({
      ...checklistData,
      assignee: genUser({ username: "user2" }),
    });

    const result = customRender(<ChecklistDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });

    expect(result.queryByText(/delectus aut autem/i)).not.toBeInTheDocument();
    expect(result.queryByText("taskItem.notChecked")).not.toBeInTheDocument();
    expect(
      result.queryByText("checklistDetail.sendReminder"),
    ).toBeInTheDocument();
  });

  it("renders breadcrumbs", () => {
    mockUseLocation.mockReturnValue({ state: { fromMyLists: false } });
    const result = customRender(<ChecklistDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });

    const breadcrumb = result.getByText("runDetail.runsLink");
    expect(breadcrumb).toBeInTheDocument();
  });

  it("renders my lists breadcrumb when coming from my lists page", () => {
    mockUseLocation.mockReturnValue({ state: { fromMyLists: true } });
    const result = customRender(<ChecklistDetail />, {
      store,
      context,
      initialEntries,
      paths,
    });

    const breadcrumb = result.getByText("userChecklist.activeListTitle");
    expect(breadcrumb).toBeInTheDocument();
  });
});
