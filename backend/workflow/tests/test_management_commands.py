"""
Tests for Workflow management commands
"""

from io import StringIO

from django.core.management import call_command

import pytest
from django_celery_beat.models import DAYS

from profiles.models import User, Team, TaggedMembership
from profiles.tests.factories import UserWithNameFactory, TeamWithTaggedUsersFactory
from workflow.models import (
    Run,
    RunTrendTaskReport,
    ChecklistDefinition,
    ChecklistTaskDefinition,
    Recurrence,
    IntervalSchedule,
    Checklist,
)
from workflow.interaction import INTERFACE_TYPE_CHOICES, NUMERIC_INTERFACE
from workflow.tests.factories import (
    ChecklistTaskDefinitionFactory,
    IntervalScheduleFactory,
    ChecklistDefinitionWithAllTaskTypes,
    RecurrenceFactory,
    CrontabScheduleFactory,
)
from workflow.tests.utils import create_team_and_checklist_definition
from workflow.management.commands.seed_data import (
    DEFAULT_USER_COUNT,
    DEFAULT_TEAM_COUNT,
)

pytestmark = [
    pytest.mark.django_db(transaction=True),
]


def test_command_create_and_compute_trends_report():
    """Test command creating (if missing) and computing trends report for recurrence"""
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    # One of these is already created by the previous call. Let's add one more.
    ChecklistTaskDefinitionFactory.create(checklist_definition=checklist_definition)
    Run.create_run_for_team(team, checklist_definition)
    assert RunTrendTaskReport.objects.count() == 2

    RunTrendTaskReport.objects.all().delete()
    assert RunTrendTaskReport.objects.count() == 0

    options = {"checklist_definition_id": checklist_definition.id}
    call_command("create_compute_trends_report", **options)
    assert RunTrendTaskReport.objects.count() == 2

    with pytest.raises(SystemExit):
        dummy_options = {"checklist_definition_id": "dummyid0"}
        call_command("create_compute_trends_report", **dummy_options)


def test_seed_data():
    """Test seed data command sets up db correctly"""
    out = StringIO()
    call_command("seed_data", stdout=out)

    assert "Created admin" in out.getvalue()
    assert User.objects.filter(is_superuser=True).count() == 1
    assert "Created team=" in out.getvalue()
    # Users plus admin
    assert User.objects.count() == DEFAULT_USER_COUNT * DEFAULT_TEAM_COUNT + 1
    assert Team.objects.count() == DEFAULT_TEAM_COUNT

    team = Team.objects.first()
    assert team.name.startswith("Team ")

    # Unique tag for each user in team
    tags = TaggedMembership.get_distinct_tags_for_teams([team.id])
    assert tags.count() == DEFAULT_USER_COUNT

    expected_checklists = 2
    assert ChecklistDefinition.objects.count() == expected_checklists
    expected_subtasks = 2
    expected_tasks = (
        len(INTERFACE_TYPE_CHOICES) + expected_subtasks
    ) * expected_checklists
    assert ChecklistTaskDefinition.objects.count() == expected_tasks
    assert Recurrence.objects.count() == 1


def _seed_data_model_counts():
    """
    Count the number of rows in each table to verify we made as much as we expected.
    """
    models = [User, Team, ChecklistDefinition, IntervalSchedule, Recurrence]
    return {str(model): model.objects.count() for model in models}


def test_seed_data_rerun():
    """Ensure that rerunning seed_data does not create extra instances"""

    call_command("seed_data")
    model_count_expected = _seed_data_model_counts()

    call_command("seed_data")
    model_count_result = _seed_data_model_counts()
    assert model_count_result == model_count_expected


def build_recurrence():
    """
    Helper factory function for building a useful recurrence for our tests.
    """
    users = UserWithNameFactory.create_batch(2)
    team = TeamWithTaggedUsersFactory.create(members=users)
    checklist_definition = ChecklistDefinitionWithAllTaskTypes(author=users[0])
    recurrence = RecurrenceFactory(
        active=False,
        interval_schedule=IntervalScheduleFactory(every=5, period=DAYS),
        checklist_definition=checklist_definition,
        team=team,
    )
    return recurrence


# Freezing time should prevent false positives from timezone.now() making slightly
# different values.
@pytest.mark.freeze_time("2020-05-01")
def test_seed_historical_data():
    """
    End-to-end test of seeding command for historical data.
    """
    recurrence = build_recurrence()
    call_command(
        "seed_historical_data", f"--recurrence={recurrence.id}", "--run-count=2"
    )
    # Two runs for two users
    assert Checklist.objects.all().count() == 4
    assert Checklist.objects.filter(completed=True).exists()
    call_command(
        "seed_historical_data", f"--recurrence={recurrence.id}", "--run-count=2"
    )
    # Should create another four checklists
    assert Checklist.objects.all().count() == 8
    runs = list(Run.objects.all())
    start_dates = [run.start_date for run in runs]
    end_dates = [run.end_date for run in runs]
    due_dates = [run.due_date for run in runs]
    assert len(start_dates) == len(set(start_dates))
    assert len(end_dates) == len(set(end_dates))
    assert len(due_dates) == len(set(due_dates))


def test_seed_historical_data_wrong_schedule_type():
    """
    Bail cleanly if the recurrence has crontab scheduling.
    """
    recurrence = build_recurrence()
    recurrence.interval_schedule = None
    recurrence.crontab_schedule = CrontabScheduleFactory()
    recurrence.save()
    stderr = StringIO()
    call_command(
        "seed_historical_data",
        f"--recurrence={recurrence.id}",
        stderr=stderr,
    )
    assert "Only recurrences with interval schedules" in stderr.getvalue()
    assert Run.objects.count() == 0


def test_seed_historical_data_useful_error_for_missing_recurrence():
    """
    Make sure we return a useful error message when we specify a nonsense recurrence.
    """
    stderr = StringIO()
    call_command(
        "seed_historical_data",
        "--recurrence=1234",
        "--run-count=2",
        stderr=stderr,
    )
    assert "Error loading Recurrence" in stderr.getvalue()


def test_delivers_useful_error():
    """
    Make sure we deliver a useful error when needed.
    """
    stderr = StringIO()
    recurrence = build_recurrence()
    task_def = recurrence.checklist_definition.task_definitions.filter(
        interface_type=NUMERIC_INTERFACE,
    ).first()
    task_def.customization_args["min_value"] = 0.5
    task_def.customization_args["step"] = 0.5
    task_def.save()
    call_command(
        "seed_historical_data",
        f"--recurrence={recurrence.id}",
        "--run-count=2",
        stderr=stderr,
    )
    assert "Only integers" in stderr.getvalue()


def test_seed_historical_data_non_compliance():
    """
    Make sure we have some checklists incomplete when compliance is low.
    """
    recurrence = build_recurrence()
    call_command(
        "seed_historical_data",
        f"--recurrence={recurrence.id}",
        "--run-count=2",
        "--compliance=0",
    )
    # Two runs for two users
    assert Checklist.objects.all().count() == 4
    assert not Checklist.objects.filter(completed=True).exists()
