import { Checkbox } from "../Checkbox";
import { render } from "../../../utils/test-utils";
import { describe, it, expect } from "vitest";

describe("Checkbox.tsx", () => {
  it("Renders a checked checkbox.", () => {
    const result = render(
      <Checkbox
        customizationArgs={null}
        response={{ model: { checked: true } }}
      />,
    );
    expect(result.queryByText("taskItem.checked")).toBeInTheDocument();
    expect(result.queryByText("taskItem.notChecked")).not.toBeInTheDocument();
  });

  it("Renders an unchecked checkbox.", () => {
    const result = render(
      <Checkbox
        customizationArgs={null}
        response={{ model: { checked: false } }}
      />,
    );
    expect(result.queryByText("taskItem.notChecked")).toBeInTheDocument();
    expect(result.queryByText("taskItem.checked")).not.toBeInTheDocument();
  });
});
