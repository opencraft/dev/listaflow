import { Dropdown, Nav } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import ThreeDotMenuIcon from "../assets/icons/three-dot-menu.svg?react";
import EditIcon from "../assets/icons/edit.svg?react";
import ArchiveIcon from "../assets/icons/archive.svg?react";
import UnarchiveIcon from "../assets/icons/unarchive.svg?react";
import ShareListIcon from "../assets/icons/share-list.svg?react";
import ListIcon from "../assets/icons/list.svg?react";
import { TaskListReview } from "../types/Task";
import useNotification from "../hooks/useNotification";
import { useTranslation } from "react-i18next";

export declare interface ChecklistActionArgs {
  onArchiveUnarchive: (id: string, isArchived: boolean) => void;
  checklist: TaskListReview;
  isOwnedByCurrentUser: boolean;
  taskReportLink: string;
  fromMyLists: boolean;
}

export const ChecklistAction = ({
  checklist,
  isOwnedByCurrentUser,
  onArchiveUnarchive,
  taskReportLink,
  fromMyLists,
}: ChecklistActionArgs) => {
  const { setNotification } = useNotification();
  const { t } = useTranslation();
  const showArchiveAction = onArchiveUnarchive !== null && isOwnedByCurrentUser;
  const copyToClipboard = (text: string) => {
    navigator.clipboard.writeText(text);
    setNotification({
      message: t("checklists.notification.copiedToClipboard"),
      variant: "success",
    });
  };
  return (
    <Dropdown>
      <Dropdown.Toggle
        className="circle-active m-auto interaction"
        as={Nav.Link}
        to="#"
        split
        id="dropdown-split-basic"
      >
        <ThreeDotMenuIcon className="base-icon" />
      </Dropdown.Toggle>
      <Dropdown.Menu className="p-2 border fs-sm">
        {!checklist.is_archived && (
          <Dropdown.Item
            className="text-reset"
            as={NavLink}
            to={`/lists/${checklist.id}/`}
            state={{ fromMyLists: fromMyLists }}
          >
            <EditIcon className="base-icon sm me-2" />
            <span className="mb-0">{t("checklists.action.view")}</span>
          </Dropdown.Item>
        )}
        {!checklist.is_archived && (
          <Dropdown.Item
            className="text-reset"
            onClick={() =>
              copyToClipboard(
                `${window.location.origin}/lists/${checklist.id}/`,
              )
            }
          >
            <ShareListIcon className="base-icon sm me-2" />
            <span className="mb-0">{t("checklists.action.copyLink")}</span>
          </Dropdown.Item>
        )}
        {taskReportLink !== "" && (
          <Dropdown.Item
            as={NavLink}
            className="text-reset"
            to={taskReportLink}
          >
            <ListIcon className="base-icon sm me-2" />
            <span className="mb-0">{t("checklists.action.showReport")}</span>
          </Dropdown.Item>
        )}
        {showArchiveAction && (
          <Dropdown.Item
            className="text-reset"
            onClick={() =>
              onArchiveUnarchive(checklist.id, !checklist.is_archived)
            }
          >
            {checklist.is_archived ? (
              <UnarchiveIcon className="base-icon sm me-2" />
            ) : (
              <ArchiveIcon className="base-icon sm me-2" />
            )}
            <span className="mb-0">
              {checklist.is_archived
                ? t("checklists.action.unarchive")
                : t("checklists.action.archive")}
            </span>
          </Dropdown.Item>
        )}
      </Dropdown.Menu>
    </Dropdown>
  );
};
