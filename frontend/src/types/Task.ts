import { User } from "./User";

export enum ChecklistArchiveAction {
  ARCHIVE = "ARCHIVE",
  UNARCHIVE = "UNARCHIVE",
}

export enum ChecklistStatus {
  UP_COMING = "UP_COMING",
  TO_DO = "TO_DO",
  IN_PROGRESS = "IN_PROGRESS",
  PAST_DUE = "PAST_DUE",
  COMPLETED = "COMPLETED",
}

export enum ChecklistListName {
  TO_DO = "TO_DO",
  ASSIGNED_TO_ME = "ASSIGNED_TO_ME",
  ALL = "ALL",
}

export enum RunArchiveAction {
  ARCHIVE = "ARCHIVE",
  UNARCHIVE = "UNARCHIVE",
}

export declare type ChecklistCount = {
  [key in ChecklistListName]: number;
};

export declare interface TaskBase<T extends string = string, C = object> {
  id: string;
  label: string;
  body: string;
  interface_type: T;
  customization_args: C;
}

export declare interface Task<
  T extends string = string,
  C extends object = object,
  R extends object = object,
> extends TaskBase<T, C> {
  completed: boolean;
  completed_on: Date;
  required: boolean;
  response: R | null;
  parent: string;
}

export declare interface Recurrence {
  recurring_schedule_display: string;
  team: string;
  checklist_definition: string;
}

declare interface RunAssignee extends User {
  completed: boolean;
}

export declare interface Run {
  id: string;
  name: string;
  start_date: string;
  end_date: string;
  due_date: string;
  team_name: string;
  is_archived: boolean;
  completed_checklist_count: number;
  total_checklist_count: number;
  assignees: RunAssignee[];
  recurrence: Recurrence;
}

export declare interface TaskList {
  id: string;
  name: string;
  body: string;
  tasks: Array<Task>;
  assignee: User;
  definition: string;
  created_on: Date;
  completed: boolean;
  force_complete: boolean;
  completed_on: Date;
  status: ChecklistStatus;
  run?: Run;
}

export declare interface TaskListReview {
  id: string;
  name: string;
  body: string;
  assignee: User;
  created_on: string;
  completed: boolean;
  completed_on: Date;
  run?: Run;
  completed_task_count: number;
  total_task_count: number;
  status: ChecklistStatus;
  is_archived: boolean;
}

export declare interface Result {
  id: string;
}

export declare interface ChecklistList {
  count: number;
  next: string | null;
  previous: string | null;
  results: Array<Result>;
}
