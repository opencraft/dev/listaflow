"""Workflow filters"""

import operator
from functools import reduce

from django_filters import rest_framework as filters
from django.db import models
from django.db.models import Q
from workflow.enums import ChecklistStatus
from workflow.models import Checklist, Run


class BaseChecklistFilter(filters.FilterSet):
    """Base filter for workflow checklists"""

    filter_statuses_use_user: bool

    usernames = filters.CharFilter(method="filter_usernames")
    statuses = filters.CharFilter(method="filter_statuses")
    is_archived = filters.BooleanFilter(field_name="is_archived")

    class Meta:
        model = Checklist
        fields = ["usernames", "statuses", "is_archived", "name"]
        filter_overrides = {
            models.CharField: {
                "filter_class": filters.CharFilter,
                "extra": lambda f: {
                    "lookup_expr": "icontains",
                },
            }
        }

    def filter_statuses(self, queryset, _, value):
        """Filter by checklist statuses"""
        if self.filter_statuses_use_user:
            user = self.request.user
        else:
            user = None
        statuses = value.split(",")
        valid_statuses: list = []
        for status in statuses:
            if status in ChecklistStatus.names():
                valid_statuses.append(status)
        return queryset.filter_by_statuses(valid_statuses, user)


class ChecklistFilter(BaseChecklistFilter):
    """Filter for workflow checklists"""

    filter_statuses_use_user = True

    def filter_usernames(self, queryset, _, value):
        """Filter by assignees usernames"""
        usernames: list[str] = value.split(",")
        return queryset.filter_by_assignees(usernames)


class RunChecklistFilter(BaseChecklistFilter):
    """Filter for workflow checklists when listed in a run"""

    filter_statuses_use_user = False

    def filter_usernames(self, queryset, _, value):
        """Filter by assignees usernames"""
        usernames: list[str] = value.split(",")
        return queryset.filter(assignee__username__in=usernames)


class RunFilter(filters.FilterSet):
    """Filter for workflow runs"""

    name = filters.CharFilter(method="filter_name")
    teams = filters.CharFilter(method="filter_dummy")
    usernames = filters.CharFilter(method="filter_dummy")

    class Meta:
        model = Run
        fields = ["is_archived"]

    def filter_name(self, queryset, _, value):
        """Filter by run name"""
        return queryset.filter(checklists__name__icontains=value)

    # pylint: disable=unused-argument
    def filter_dummy(self, queryset, name, value):
        """
        Do nothing, for filters with specialised implementation in
        `filter_queryset()`
        """
        return queryset

    def filter_queryset(self, queryset):
        """
        Filter the queryset with all filters.

        This implementation filters by `usernames` OR `teams`.
        """
        queryset = super().filter_queryset(queryset)

        data = self.form.cleaned_data
        usernames = data.get("usernames")
        team_ids = data.get("teams")
        or_filters = []
        if usernames:
            or_filters.append(
                Q(checklists__assignee__username__in=usernames.split(","))
            )
        if team_ids:
            or_filters.append(Q(team__id__in=team_ids.split(",")))

        if or_filters:
            queryset = queryset.filter(reduce(operator.or_, or_filters))

        return queryset
