"""Type utility for Django"""

from enum import Enum


class DjangoChoiceEnum(Enum):
    """Enumeration that provides convenient methods for Django"""

    def __str__(self):
        return self.name

    @classmethod
    def choices(cls):
        """Render enum as tuple to use in Django choice field"""
        return tuple((prop.name, prop.value) for prop in cls)

    @classmethod
    def names(cls):
        """Return enum as list of string names"""
        return list(prop.name for prop in cls)
