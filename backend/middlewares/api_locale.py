"""
Handle locale from request for API
"""

from django.conf import settings
from django.utils import translation
from django.utils.deprecation import MiddlewareMixin


class ApiLocaleMiddleware(MiddlewareMixin):  # pylint: disable=too-few-public-methods
    """
    Middleware helps set locale for request
    """

    def process_request(self, request):
        """
        Set request's language code based on content language information
        in header
        """
        lang_code = settings.LANGUAGE_CODE
        for k in request.headers:
            if k.lower() == settings.API_LANGUAGE_HEADER:
                lang_code = request.headers.get(k)
                break
        translation.activate(lang_code)
        request.LANGUAGE_CODE = translation.get_language()

        return self.get_response(request)
