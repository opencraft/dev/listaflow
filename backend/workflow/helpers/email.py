"""helpers for sending email"""

import json
from datetime import date, timedelta
from typing import List, Literal, Optional, TYPE_CHECKING
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.mail.backends.base import BaseEmailBackend
from django.core.serializers.json import DjangoJSONEncoder
from django.template.loader import get_template
from django.utils import timezone
from jinja2.sandbox import SandboxedEnvironment
from jinja2 import TemplateSyntaxError

from lib.constants import ERROR
from lib.helpers import log_event
from workflow.models import Email


if TYPE_CHECKING:
    from workflow.models import Checklist, Run

sandboxed_env = SandboxedEnvironment()


def _get_run_context(run_instance: "Run"):
    """
    Return run related details like end_date, due_date, etc.
    """
    context = {
        "end_date": run_instance.end_date.date(),
        "due_date": run_instance.due_date,
        "start_date": run_instance.start_date.date(),
    }

    def _get_date_details(run_date: date, key_prefix=""):
        """
        Returns details of date like day of week, month, date etc.
        """
        date_context = {
            f"{key_prefix}month_name": run_date.strftime("%b"),
            f"{key_prefix}day_of_month": str(run_date.day),
            f"{key_prefix}day_of_week": run_date.strftime("%A"),
            f"{key_prefix}year": str(run_date.year),
        }
        return date_context

    context.update(_get_date_details(run_instance.end_date, key_prefix="run_end_"))
    context.update(_get_date_details(run_instance.due_date, key_prefix="due_date_"))
    context.update(_get_date_details(run_instance.start_date, key_prefix="run_start_"))
    return context


def template_from_string(template_string: str):
    """
    Convert a string into a Jinja2 template object using SandboxedEnvironment.
    Ensures that no dangerous operations can be performed in the template.
    """
    try:
        return sandboxed_env.from_string(template_string)
    except TemplateSyntaxError as error:
        # pylint: disable=no-value-for-parameter
        raise TemplateSyntaxError(f"Invalid template syntax: {error}") from error


def _get_templates_or_default(
    email_type: Literal["reminder", "notification"],
    email_instance: Optional[Email] = None,
):
    """
    gets a template object for given recurrence_instance if present from
    database else uses default templates
    """
    html_body_template = get_template(f"email/checklist_{email_type}/body.html")
    text_body_template = get_template(f"email/checklist_{email_type}/body.txt")
    subject_template = get_template(f"email/checklist_{email_type}/subject.txt")
    if email_instance:
        if email_instance.html_body:
            html_body_template = template_from_string(email_instance.html_body)
        if email_instance.text_body:
            text_body_template = template_from_string(email_instance.text_body)
        if email_instance.subject:
            subject_template = template_from_string(email_instance.subject)
    return subject_template, text_body_template, html_body_template


def ensure_safe_context(context):
    """
    Validates the context by serializing it using DjangoJSONEncoder
    to ensure it contains only safe, serializable data.
    Raises a ValueError if non-serializable objects are found.
    """
    try:
        json.dumps(context, cls=DjangoJSONEncoder)
    except (TypeError, ValueError) as e:
        raise ValueError(f"Unsafe object in context: {str(e)}") from e


def _get_rendered_email_message(
    context: dict,
    email_type: Literal["reminder", "notification"],
    email_instance: Optional[Email] = None,
):
    """
    Returns rendered email message from db if dynamic email template for given
    recurrence exists else returns default templates Also, takes care of
    selecting between notification and reminder templates based on due_date
    """
    try:
        (
            subject_template,
            text_body_template,
            html_body_template,
        ) = _get_templates_or_default(email_type, email_instance)
        ensure_safe_context(context)
        email_subject = subject_template.render(context)
        email_text_body = text_body_template.render(context)
        email_html_body = html_body_template.render(context)

        return email_subject, email_text_body, email_html_body
    except TemplateSyntaxError as e:
        with log_event(
            event_type="email_template_error",
            level=ERROR,
            data={"error": str(e), "context": context, "email_type": email_type},
        ):
            return None, None, None


# pylint: disable=too-many-arguments, too-many-positional-arguments
def _send_pending_checklist_notification_mails(
    email: str,
    checklist_context: dict,
    run_context: dict,
    conn: BaseEmailBackend,
    email_type: Literal["reminder", "notification"],
    email_instance: Optional[Email] = None,
):
    """Sends pending checklist notification to the given email.

    Args:
        email: str. The email of the user.
        checklist_context: dict. Should contain display_name, checklist_name,
                           checklist_id and task_map
        run_context: dict. Should contain start_date, end_date & due_date
        conn: email backend connection
        email_type: "notification" if there is some time before the due date, or
                    "reminder" if the due date has arrived.
    """
    context = {"site_url": settings.FRONTEND_URL}
    context.update(run_context)
    context.update(checklist_context)
    # Good to have a function to check the context to ensure it is sanitised of any
    # potentially unsafe types or objects.
    (
        email_subject,
        email_text_body,
        email_html_body,
    ) = _get_rendered_email_message(
        context,
        email_type,
        email_instance,
    )
    # Technically, with certain very strange RFC-compliant emails with <s, this could
    # fail, but Django doesn't allow you to save those cases in email fields to begin
    # with.
    # "Listaflow <noreply@listaflow.com>" -> "noreply@listaflow.com"
    from_email = settings.DEFAULT_FROM_EMAIL.strip().split("<")[-1][:-1]
    from_user_email = context["from_user_email"]
    if from_user_email:
        reply_to = [f"{context["from_name"]} <{from_user_email}>"]
    else:
        reply_to = settings.DEFAULT_REPLY_TO_EMAILS
    mail_obj = EmailMultiAlternatives(
        email_subject.strip(),
        email_text_body,
        f'{context["from_name"]} <{from_email}>',
        [email],
        reply_to=reply_to,
        connection=conn,
    )
    mail_obj.attach_alternative(email_html_body, "text/html")
    mail_obj.send()


def send_pending_checklist_email_for_runs(
    run_instance: "Run",
    conn: BaseEmailBackend,
    days_pending: timedelta,
    checklists: Optional[List["Checklist"]] = None,
):
    """Sends pending checklist notification to the assignee.

    Args:
        run_instance: Instance of Run model.
        conn: email backend conn
        days_pending: timedelta between due_date and today
        checklists: optional list of checklists, defaults to all
            non-completed checklists of the run
    """
    run_context = _get_run_context(run_instance)
    if checklists is None:
        checklists = run_instance.checklists.filter(completed=False, is_archived=False)
    for checklist_instance in checklists:
        if run_instance.recurrence.from_user:
            from_name = run_instance.recurrence.from_user.display_name
            from_user_email = run_instance.recurrence.from_user.email
        else:
            from_name = run_instance.team.name
            from_user_email = None
        checklist_context = {
            "display_name": checklist_instance.assignee.display_name,
            "checklist_name": checklist_instance.name,
            "checklist_id": checklist_instance.id,
            "task_map": checklist_instance.task_names_with_children(),
            "from_name": from_name,
            "from_user_email": from_user_email,
        }
        email = checklist_instance.assignee.email
        if days_pending > timezone.timedelta(1):
            email_type: Literal["notification"] = "notification"
            email_instance = run_instance.recurrence.notification_email
        else:
            email_type: Literal["reminder"] = "reminder"
            email_instance = run_instance.recurrence.reminder_email
        _send_pending_checklist_notification_mails(
            email,
            checklist_context,
            run_context,
            conn=conn,
            email_type=email_type,
            email_instance=email_instance,
        )
