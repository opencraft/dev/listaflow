import { act, fireEvent, render, screen } from "../../../utils/test-utils";
import { vi, describe, beforeEach, expect, it } from "vitest";
import { PopupDateRangePicker } from "../PopupDateRangePicker";

const onChangeTestFunc = vi.fn();

const TestComponent = () => {
  const currentSelected = { startDate: null, endDate: null };

  return (
    <PopupDateRangePicker
      onDateRangeChange={onChangeTestFunc}
      currentSelected={currentSelected}
    />
  );
};

describe("PopupDateRangePicker", () => {
  beforeEach(() => {
    render(<TestComponent />);
  });

  it("should render date button", async () => {
    const button = screen.getByText("reports.dateRange.last12Months");
    expect(button).toBeInTheDocument();
  });

  it("should render calendar on click", async () => {
    const button = screen.getByText("reports.dateRange.last12Months");
    act(() => {
      fireEvent.click(button);
    });
    const calendar = screen.getByTitle("reports.dateRange.chooseDates");
    expect(calendar).toBeInTheDocument();
  });

  it("should call onChange on date range change", async () => {
    const button = screen.getByText("reports.dateRange.last12Months");
    act(() => {
      fireEvent.click(button);
    });
    const lastMonthButton = screen.getByText("reports.dateRange.lastMonth");
    act(() => {
      fireEvent.click(lastMonthButton);
    });
    expect(onChangeTestFunc).toHaveBeenCalled();
  });
});
