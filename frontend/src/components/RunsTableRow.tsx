import { Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { AssigneesCard } from "./AssigneesCard";
import { RunAction } from "./RunAction";
import RunIcon from "../assets/icons/run.svg?react";
import { Run } from "../types/Task";
import { format, parseISO } from "date-fns";
import { REPORTS_COMPARE } from "../constants/urls";
import { useTranslation } from "react-i18next";
import { RunSubtitle } from "./RunSubtitle";

declare interface RunsTableRowArgs {
  run: Run;
  onArchiveUnarchive: (id: string, isArchived: boolean) => void;
}

export const RunsTableRow = ({ run, onArchiveUnarchive }: RunsTableRowArgs) => {
  const { t } = useTranslation();

  const detailLink = `/runs/${run.id}/`;

  const reportQueryParams = new URLSearchParams({
    checklistDefinition: run.recurrence.checklist_definition,
    team_ids: run.recurrence.team,
    start_date: format(parseISO(run.start_date), "yyyy-MM-dd"),
    end_date: format(parseISO(run.end_date), "yyyy-MM-dd"),
  });
  const taskReportLink = `${REPORTS_COMPARE}?${reportQueryParams.toString()}`;

  return (
    <Row key={run.id} className="item mb-3 me-0 ms-0 fs-sm run-listing-item">
      <Link to={detailLink} className={"link-overlay rounded"}>
        <span className={"d-none"}>{run.name}</span>
      </Link>
      <Col className="d-none d-sm-block ms-2 pe-0" sm="auto">
        <RunIcon className="base-icon" height={30} />
      </Col>
      <Col>
        <Row className="fs-8">
          <Col>
            <span className="fw-bold fs-6 text-white">{run.name}</span>
          </Col>
          <Col className="d-none d-sm-block" sm="auto">
            <AssigneesCard
              id={run.id}
              assignees={run.assignees.filter((assignee) => assignee.completed)}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <RunSubtitle run={run} />
          </Col>
          <Col xs={12} sm="auto">
            <span className="noselect">
              {t("runs.table.row.progress", {
                completed: run.completed_checklist_count,
                total: run.total_checklist_count,
              })}
            </span>
          </Col>
        </Row>
      </Col>
      <Col className="pe-0 py-0" xs="auto">
        <div className="vr h-100" />
      </Col>
      <Col className="px-0" xs="auto">
        <RunAction
          onArchiveUnarchive={onArchiveUnarchive}
          run={run}
          taskReportLink={taskReportLink}
        />
      </Col>
    </Row>
  );
};
