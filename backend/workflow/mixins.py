"""
Mixin classes to help with better organization and maitainance of code.
"""

from django.db import models
from django_better_admin_arrayfield.models.fields import ArrayField
from short_stuff import gen_shortcode
from short_stuff.django.models import ShortCodeField


def get_default_reminder_days():
    """
    Setting the first default reminder.
    """
    return [0]


class BaseReminderModel(models.Model):
    """
    Base reminder class that includes the reminder days field.
    """

    id = ShortCodeField(primary_key=True, db_index=True, default=gen_shortcode)
    reminders = ArrayField(
        models.PositiveIntegerField(),
        blank=True,
        default=get_default_reminder_days,
        help_text="Days before due date a reminder should be sent",
    )

    class Meta:
        abstract = True
