import { customRender } from "../../utils/test-utils";
import { Run } from "../../types/Task";
import { RunsTableRow } from "../RunsTableRow";
import * as RunAction from "../RunAction";
import { expect, vi, describe, it } from "vitest";

vi.mock("../RunAction", () => ({
  RunAction: () => <div data-testid="run-action" />,
}));

describe("RunsTableRow.tsx", () => {
  const run = {
    id: "test_run_id",
    name: "Test Run",
    is_archived: false,
    assignees: [],
    team_name: "Test Team",
    start_date: "2024-03-07T12:00:00",
    end_date: "2024-03-08T12:00:00",
    due_date: "2024-03-09T12:00:00",
    completed_checklist_count: 5,
    total_checklist_count: 10,
    recurrence: {
      id: "test_recurrence_id",
      periodic_task: 3,
      recurring_schedule_display: "Every 5 minutes",
      team: "test_team_id",
      checklist_definition: "test_checklist_definition_id",
    },
  };

  it("Displays run information", async () => {
    const runActionSpy = vi.spyOn(RunAction, "RunAction");
    const onArchiveUnarchive = vi.fn();
    const result = customRender(
      <RunsTableRow run={run} onArchiveUnarchive={onArchiveUnarchive} />,
    );
    expect(result.getByTestId("run-action")).toBeTruthy();
    expect(runActionSpy).toHaveBeenCalledWith(
      {
        run: run,
        onArchiveUnarchive: onArchiveUnarchive,
        taskReportLink:
          "/reports/compare?checklistDefinition=test_checklist_definition_id&" +
          "team_ids=test_team_id&start_date=2024-03-07&end_date=2024-03-08",
      },
      {},
    );
  });
});
