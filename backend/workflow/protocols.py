"""
Protocols for typing
"""

# pylint: disable=too-few-public-methods
from typing import Protocol

from django.contrib.auth.base_user import AbstractBaseUser


class AuthoredModel(Protocol):
    """Protocol for models with an author field."""

    author: AbstractBaseUser


class AssignedModel(Protocol):
    """Protocol for models with an assignee field."""

    assignee: AbstractBaseUser
