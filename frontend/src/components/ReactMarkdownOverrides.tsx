import { Components } from "react-markdown";

const components: Components = {
  li: ({ children }) => (
    <li>
      <bdi>{children}</bdi>
    </li>
  ),
  em: ({ children }) => (
    <em>
      <bdi>{children}</bdi>
    </em>
  ),
  strong: ({ children }) => (
    <strong>
      <bdi>{children}</bdi>
    </strong>
  ),
  span: ({ children }) => (
    <span>
      <bdi>{children}</bdi>
    </span>
  ),
  p: ({ children }) => (
    <p>
      <bdi>{children}</bdi>
    </p>
  ),
};

export default components;
