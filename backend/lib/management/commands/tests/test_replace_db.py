from unittest.mock import patch, MagicMock

import pytest
from django.core.management import call_command, CommandError
from django.test import override_settings


def get_mock_execute(mock_connection: MagicMock):
    return mock_connection.cursor.return_value.__enter__.return_value.execute


@patch("lib.management.commands.replace_db.call_command")
@patch("lib.management.commands.replace_db.connection")
@override_settings(ENV_NAME="Development")
def test_replace_db(mock_connection, mock_call_command):
    """
    This doesn't test much, since we can't pull the db out from under ourselves,
    but we at least make sure we're not crashing from the invocation.
    """
    call_command("replace_db")
    mock_call_command.assert_called_once()
    mock_call_command.assert_called_with("dbshell")
    mock_execute = get_mock_execute(mock_connection)
    assert "Provided that these terms" in mock_execute.call_args_list[0][0][0]


@patch("lib.management.commands.replace_db.call_command")
@patch("lib.management.commands.replace_db.connection")
@override_settings(ENV_NAME="Production")
def test_replace_db_wrong_environment(mock_connection, mock_call_command):
    """
    This doesn't test much, since we can't pull the db out from under ourselves,
    but we at least make sure we're not crashing from the invocation.
    """
    with pytest.raises(CommandError):
        call_command("replace_db")
    mock_call_command.assert_not_called()
    mock_execute = get_mock_execute(mock_connection)
    mock_execute.assert_not_called()
